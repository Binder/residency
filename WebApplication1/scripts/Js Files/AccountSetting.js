﻿$(document).ready(function () {


    if ($("#canEdit").val().toUpperCase() == "FALSE") {
        $(".txt").attr("disabled", "disabled");
        $(".drop").attr("disabled", "disabled");
        $(".InCaseEditTrue").hide();
        $(".InCaseEditFlase").show();
    }
    else {
        $(".InCaseEditTrue").show();
        $(".InCaseEditFlase").hide();
    }

    $("#btnCanEdit_Submit").click(function () {
        $(".txt").removeAttr("disabled");
        $(".drop").removeAttr("disabled");
        $(".InCaseEditTrue").show();
        $(".InCaseEditFlase").hide();
    });


    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    $("#btnAccount_Submit").click(function () {
        $("#spn_EmailMessage").text("");
        if ($("#strEmailContact").val().trim() != "") {
            var IsValid = validateEmail($("#strEmailContact").val());
            if (IsValid == false) {
                $("#spn_EmailMessage").text("Plese enter valid email.")
                return false;
            }
        }
    });

    $("#strEmailContact").blur(function () {
        $("#spn_EmailMessage").text("");
        if ($("#strEmailContact").val().trim() != "") {
            var IsValid = validateEmail($("#strEmailContact").val());
            if (IsValid == false) {
                $("#spn_EmailMessage").text("Plese enter valid email.")
                return true;
            }
        }
    });

    $("#btnAccount_Clear").click(function () {
        $(".txt").val("");
        $(".drop").val("0");

    });

});