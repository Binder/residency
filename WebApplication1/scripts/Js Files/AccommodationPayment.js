﻿$(function () {
    var dateToday = new Date();

    $("#AdmissionsDate1").datepicker({ dateFormat: 'dd-mm-yy', minDate: dateToday });

    $("#ProjectPaymentDecDate1").datepicker({ dateFormat: 'dd-mm-yy', minDate: dateToday });

    $("#ActualPaymentDate1").datepicker({ dateFormat: 'dd-mm-yy', minDate: dateToday });

    $('.radioAccomodation').change(function () {

        if ($(this).val().toLowerCase() === "true") {
            $("#AccomPayment").val(true);
            $('#dvAccomoPayment').show();

        } else if ($(this).val().toLowerCase() === "false") {

            $("#AccomPayment").val(false);
            $('#dvAccomoPayment').hide();
        }

    });

    if ($("#AccomPayment").val().toLowerCase() === "true") {
        $('#dvAccomoPayment').show();
    } else {
        $('#dvAccomoPayment').hide();

    }
});