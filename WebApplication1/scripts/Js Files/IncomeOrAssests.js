﻿$(document).ready(function () {
    $("#GovtIncomeSuppPayment").focus();

    if ($('#DeclineToDeclare').is(':checked')) {
        $(".declined-to-declare").hide();
    } else {
        $(".declined-to-declare").show();
    }

    $("#DeclineToDeclare").click(function () {
        if ($('#DeclineToDeclare').is(':checked')) {
            $(".declined-to-declare").hide();
        } else {
            $(".declined-to-declare").show();
        }
    });

    $(".CBPension").click(function () {
        selectedBox = this.id;

        $(".CBPension").each(function () {
            if (this.id == selectedBox) {
                this.checked = true;
            }
            else {
                this.checked = false;
            };
        });
    });


    // Setup form validation on the #register-form element
    $("#frmincomeassests").validate({

        // Specify the validation rules
        rules: {

            GovtIncomeSuppPayment: "required",
            OtherIncome: "required",
            YourIncome: "required",
            NetRetirementVillageEntCont: "required",
            AccommodationBond: "required",
            FinancialAccounts: "required",
            Shares: "required",
            ManagedInvestment: "required",
            AssessableIncomeStreams: "required",
            RealEstateAndBusinessInt: "required",
            PrivateTrusts: "required",
            GiftsOrDeprivation: "required",
            DebtsPersonalLoans: "required",
            DebtsOther: "required",
            AsstRedAmtPrisonerOfWar: "required"
        },
        // Specify the validation error messages
        messages: {

            GovtIncomeSuppPayment: "field required",
            OtherIncome: "field required",
            YourIncome: "field required",
            NetRetirementVillageEntCont: "field required",
            AccommodationBond: "field required",
            FinancialAccounts: "field required",
            Shares: "field required",
            ManagedInvestment: "field required",
            AssessableIncomeStreams: "field required",
            RealEstateAndBusinessInt: "field required",
            PrivateTrusts: "field required",
            GiftsOrDeprivation: "field required",
            DebtsPersonalLoans: "field required",
            DebtsOther: "field required",
            AsstRedAmtPrisonerOfWar: "field required"
        },

        submitHandler: function (form) {
            form.submit();
        }
    });


    function sum() {

        //Asset Income = 0;
        var resultAsset = 0;
        resultAsset = resultAsset + parseFloat($("#GovtIncomeSuppPayment").val() == '' ? 0 : $("#GovtIncomeSuppPayment").val());
        resultAsset = resultAsset + parseFloat($("#OtherIncome").val() == '' ? 0 : $("#OtherIncome").val());
        resultAsset = resultAsset + parseFloat($("#YourIncome").val() == '' ? 0 : $("#YourIncome").val());
        resultAsset = resultAsset + parseFloat($("#NetRetirementVillageEntCont").val() == '' ? 0 : $("#NetRetirementVillageEntCont").val());
        resultAsset = resultAsset + parseFloat($("#AccommodationBond").val() == '' ? 0 : $("#AccommodationBond").val());
        resultAsset = resultAsset + parseFloat($("#FinancialAccounts").val() == '' ? 0 : $("#FinancialAccounts").val());
        resultAsset = resultAsset + parseFloat($("#Shares").val() == '' ? 0 : $("#Shares").val());
        resultAsset = resultAsset + parseFloat($("#ManagedInvestment").val() == '' ? 0 : $("#ManagedInvestment").val());
        resultAsset = resultAsset + parseFloat($("#AssessableIncomeStreams").val() == '' ? 0 : $("#AssessableIncomeStreams").val());
        resultAsset = resultAsset + parseFloat($("#ForeginAssets").val() == '' ? 0 : $("#ForeginAssets").val());
        resultAsset = resultAsset + parseFloat($("#RealEstateAndBusinessInt").val() == '' ? 0 : $("#RealEstateAndBusinessInt").val());
        resultAsset = resultAsset + parseFloat($("#PrivateTrusts").val() == '' ? 0 : $("#PrivateTrusts").val());
        resultAsset = resultAsset + parseFloat($("#GiftsOrDeprivation").val() == '' ? 0 : $("#GiftsOrDeprivation").val());

        resultAsset = isNaN(resultAsset) ? 0 : resultAsset;

        $("#NetAssets").val(resultAsset.toFixed(2));
        //Net Debit
        var resultDebt = 0;

        console.log($("#DebtsPersonalLoans").val());
        console.log($("#DebtsOther").val());
        resultDebt = resultDebt + parseFloat($("#DebtsPersonalLoans").val() == '' ? 0 : $("#DebtsPersonalLoans").val());
        resultDebt = resultDebt + parseFloat($("#DebtsOther").val() == '' ? 0 : $("#DebtsOther").val());

        resultDebt = isNaN(resultDebt) ? 0 : resultDebt;

        $("#NetDebts").val(resultDebt.toFixed(2));
        //Net Total Income
        $("#NetIncome").val( (resultAsset - resultDebt).toFixed(2));

    }


    $('.income-section input').keyup(function () {

        var ex = /^[0-9]+\.?[0-9]*$/;
        if (ex.test(this.value) == false) {
            this.value = this.value.substring(0, this.value.length - 1);
        } else {
            sum();
        }
    });
    $('#PensionNumber').keyup(function () {
        var ex = /^[0-9]+\.?[0-9]*$/;
        if (ex.test(this.value) == false) {
            this.value = this.value.substring(0, this.value.length - 1);
        } else {
            sum();
        }
    });
    $('#AsstRedAmtPrisonerOfWar').keyup(function () {
        var ex = /^[0-9]+\.?[0-9]*$/;
        if (ex.test(this.value) == false) {
            this.value = this.value.substring(0, this.value.length - 1);
        } else {
            sum();
        }
    });

    //AsstRedAmtPrisonerOfWar
    $('.debts-section input').keyup(function () {
        var ex = /^[0-9]+\.?[0-9]*$/;
        if (ex.test(this.value) == false) {
            this.value = this.value.substring(0, this.value.length - 1);
        } else {
            sum();
        }

    });
});