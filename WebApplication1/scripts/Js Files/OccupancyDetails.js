﻿$(document).ready(function () {
    $("#AgreedEntryDate").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#PeriodOfPreEntryFrom").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#PeriodOfPreEntryTo").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#DateCareServiceCommmenced").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#DateCareServiceCeased").datepicker({ dateFormat: 'dd-mm-yy' });

    $('.radioTransferred').change(function () {
        if ($(this).val().toLowerCase() === "true") {
            $('#DvCareService').show();
            $("#Transferred").val(true);
        } else if ($(this).val().toLowerCase() === "false") {
            $('#DvCareService').hide();
            $("#Transferred").val(false);
        }
    });
  
    if ($("#Transferred").val().toLowerCase() == "true") {

        $(".radioTransferred").each(function () {
            if ($(this).val().toLowerCase() === "true") {
                $(this).checked = true;
            }
        });
        $("#DvCareService").show();

    } else {
        $('.radioTransferred').each(function () {
            if ($(this).val().toLowerCase() === "false") {
                $(this).checked = true;
            }
        });

        $("#DvCareService").hide();
    }

    $('.rdPreEntryLeaves').change(function () {
        if ($(this).val().toLowerCase() === "true") {
            $('#dvPreEntryLeave').show();
            $("#PreEntryLeave").val(true);
        } else if ($(this).val().toLowerCase() === "false") {
            $('#dvPreEntryLeave').hide();
            $("#PreEntryLeave").val(false);
        }
    });

    if ($("#PreEntryLeave").val().toLowerCase() == "true") {

        $(".rdPreEntryLeaves").each(function () {
            if ($(this).val().toLowerCase() === "true") {
                $(this).checked = true;
            }
        });
        $("#dvPreEntryLeave").show();

    } else {
        $('.rdPreEntryLeaves').each(function () {
            if ($(this).val().toLowerCase() === "false") {
                $(this).checked = true;
            }
        });

        $("#dvPreEntryLeave").hide();
    }

   
   // Setup form validation on the #register-form element
    $("#frmoccupancy").validate({

        // Specify the validation rules
        rules: {

            Wing: "required",
            AgreedEntryDate: "required",
            PeriodOfPreEntryFrom: "required",
            PeriodOfPreEntryTo: "required",
            DateCareServiceCommmenced: "required",
            DateCareServiceCeased: "required"
        },
        // Specify the validation error messages
        messages: {
            Wing: "please enter room",
            AgreedEntryDate: "please enter agreed entry date",
            PeriodOfPreEntryFrom: "please enter leave from",
            PeriodOfPreEntryTo: "please enter leave to",
            DateCareServiceCommmenced: "please enter date care and services commenced with other provider",
            DateCareServiceCeased: "please enter date care and services ceased with other provider"
        },

        submitHandler: function (form) {
            form.submit();
        }
    });
});