﻿$(document).ready(function () {
    $("#btn_Layout_Back").hide();
    $("#userName").focus();
    //$("#btn_Submit").click(function () {
    //    var msg = "";
    //    if ($("#userName").val().trim() == "")
    //    {
    //        msg = "* Please enter user name.";
    //    }
    //    if ($("#password").val().trim() == "") {
    //        msg += "<br/>* Please enter password.";
    //    }
    //    if (msg.trim() != "")
    //    {
    //        $("#error_msg").html(msg);
    //        return false;
    //    }
    //});

    // Setup form validation on the #register-form element
    $("#frmLogin").validate({

        // Specify the validation rules
        rules: {
            userName: "required",
            password: "required"
        },

        // Specify the validation error messages
        messages: {
            userName: "Please enter user name",
            password: "Please enter password"
        },

        submitHandler: function (form) {
            form.submit();
        }
    });

    $('#userName').keyup(function () {
        if (this.value.match(/[^a-zA-Z]/g)) {
            this.value = this.value.replace(/[^a-zA-Z]/g, '');
        }
    });
});


