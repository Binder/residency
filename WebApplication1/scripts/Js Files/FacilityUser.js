﻿$(document).ready(function () {
    $("#strUserName").focus();
   
    $("#btnSignup_Cancel").click(function () {
        window.location.href = "/FacilitiesForms/FacilitySetting?FacilityID=" + getParameterByName("FacilityID");
        return false;
    });


    //$("#btnSignup_Submit").click(function () {
    //    var msg = "";
    //    if ($("#strUserName").val().trim() == "") {
    //        msg = "* Please enter user name.";
    //    }
    //    if ($("#strPassowrd").val().trim() == "") {
    //        msg += "<br/>* Please enter password.";
    //    }
    //    if ($("#emailAddress").val().trim() == "") {
    //        msg += "<br/>* Please enter email address.";
    //    }
    //    if (validateEmail($("#emailAddress").val().trim()) == false) {
    //        msg += "<br/>* Please enter valid email address.";
    //    }


    //    if (msg.trim() != "") {
    //        $("#error_msg").html(msg);
    //        return false;
    //    }
    //});

    $("#btn_ChangePassword").click(function () {
        var getPassowrd = $("#txt_Password").val().trim();
        if (getPassowrd != "") {
            var URLValue = "/Home/updatePassword?ID=" + getParameterByName("ID") + "&strpassword=" + getPassowrd;
            $.ajax({
                url: URLValue,
                type: "Get",
                async: false, //blocks window close
                success: function (data) {
                    if (data == "1") {
                        $("#txt_Password").val("");
                        alert("Password is successfully updated");
                    }
                    else { alert("server error, please try after some time!!") }
                }
            });

        }

    });

        // Setup form validation on the #register-form element
        $("#frmLogin").validate({

            // Specify the validation rules
            rules: {
                strUserName: "required",
                strPassowrd: "required",
                emailAddress: {
                    required: true,
                    email: true
                }
            },

            // Specify the validation error messages
            messages: {
                strUserName: "Please enter your first name",
                strPassowrd: "Please enter password",
                emailAddress: "Please enter a valid email address"
            },

            submitHandler: function (form) {
                form.submit();
            }
        });

 
        //$('#txtNumeric').keyup(function() {
        //    if (this.value.match(/[^0-9]/g)) {
        //        this.value = this.value.replace(/[^0-9]/g, '');
        //    }
        //});
        $('#strUserName').keyup(function () {
            if (this.value.match(/[^a-zA-Z]/g)) {
                this.value = this.value.replace(/[^a-zA-Z]/g, '');
            }
        });
        //$('#txtAlphaNumeric').keyup(function() {
        //    if (this.value.match(/[^a-zA-Z0-9]/g)) {
        //        this.value = this.value.replace(/[^a-zA-Z0-9]/g, '');
        //    }
        //});
    });

   




