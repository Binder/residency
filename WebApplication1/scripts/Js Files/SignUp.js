﻿$(document).ready(function () {
    $("#strUserName").focus();

    $("#btn_ChangePassword").click(function () {
        var getPassowrd = $("#txt_Password").val().trim();
        if (getPassowrd != "") {
            var URLValue = "/Home/updatePassword?ID=" + getParameterByName("ID") + "&strpassword=" + getPassowrd;
            $.ajax({
                url: URLValue,
                type: "Get",
                async: false, //blocks window close
                success: function (data) {
                    if (data == "1") {
                        $("#txt_Password").val("");
                        alert("Password is successfully updated");
                    }
                    else { alert("server error, please try after some time!!") }
                }
            });

        }

    });


    
    //user validation

    //jQuery.validator.addMethod("alphanumeric", function (value, element) {
    //    return this.optional(element) || /^\w+$/i.test(value);
    //}, "Letters, numbers, and underscores only please");



    jQuery.validator.addMethod(
"textonly",function (value, element) {
    console.log('textonly called.');
    console.log(/[-.\'\w\s]/.test(value));
    return this.optional(element) || /[-.\'\w\s]/.test(value);
}
//jQuery.format("Please only enter letters, spaces, periods, or hyphens.")
);

    //user validation




        // Setup form validation on the #register-form element
        $("#frmLogin").validate({

            // Specify the validation rules
            rules: {
                strUserName: {
                    required: true,
                    textonly: true
                   
                },

                


                emailAddress: {
                    required: true,
                    email: true
                },
                //strPassowrd: "required",

                
                strPassowrd: "required",
                noOfFacility: "required"
            },

            // Specify the validation error messages
            messages: {
                //strUserName: "Please enter your first name",

                "strUserName": {
                    required: "Please enter your first name",
                    textonly: "Login format not valid"
                    
                },
                emailAddress: "Please enter a valid email address",
                strPassowrd: "Please enter password",
                noOfFacility: "Please enter facility value"
            },

            submitHandler: function (form) {
                form.submit();
            }
        });

      

        //$('#strUserName').keyup(function () {
        //    if (this.value.match(/[^a-zA-Z]/g)) {
        //        this.value = this.value.replace(/[^a-zA-Z]/g, '');
        //    }
        //});
        $('#noOfFacility').keyup(function() {
                if (this.value.match(/[^0-9]/g)) {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }
        });












    //password js

       
 
            $('#strPassowrd').keyup(function(){
                $('#result').html(checkStrength($('#strPassowrd').val()))
            })  
 
            function checkStrength(password){
 
                //initial strength
                var strength = 0
 
                //if the password length is less than 6, return message.
                if (password.length < 6) {
                    $('#result').removeClass()
                    $('#result').addClass('short')
                    return 'Too short'
                }
 
                //length is ok, lets continue.
 
                //if length is 8 characters or more, increase strength value
                if (password.length > 7) strength += 1
 
                //if password contains both lower and uppercase characters, increase strength value
                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
 
                //if it has numbers and characters, increase strength value
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
 
                //if it has one special character, increase strength value
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
 
                //if it has two special characters, increase strength value
                if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength += 1
 
                //now we have calculated strength value, we can return messages
 
                //if value is less than 2
                if (strength < 2 ) {
                    $('#result').removeClass()
                    $('#result').addClass('weak')
                    return 'Weak'
                } else if (strength == 2 ) {
                    $('#result').removeClass()
                    $('#result').addClass('good')
                    return 'Good'
                } else {
                    $('#result').removeClass()
                    $('#result').addClass('strong')
                    return 'Strong'
                }
            }

    //password





        
      
});
