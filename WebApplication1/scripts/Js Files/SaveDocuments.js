﻿$(document).ready(function () {

    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    //Next
    $("#a_Next").click(function () {
        var elements = "";
        $("#tabstrip ul li").each(function (index) {
            var $this = $(this);
            var ifTrue = $this.attr("aria-selected");
            if (ifTrue == "true") {
                elements = $this.attr("aria-controls");
                if (elements != undefined) {
                    return false;
                }
            }
        });
        var splValues = parseInt(elements.split('-')[1]) + 1;
        $('[aria-controls="tabstrip-' + splValues + '"]').click();
    });

    //Previous
    $("#a_Pre").click(function () {
        var elements = "";
        $("#tabstrip ul li").each(function (index) {
            var $this = $(this);
            var ifTrue = $this.attr("aria-selected");
            if (ifTrue == "true") {
                elements = $this.attr("aria-controls");
                if (elements != undefined) {
                    return false;
                }
            }
        });
        var splValues = parseInt(elements.split('-')[1]) - 1;
        $('[aria-controls="tabstrip-' + splValues + '"]').click();
    });




});