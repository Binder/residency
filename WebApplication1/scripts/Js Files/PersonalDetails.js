﻿$(document).ready(function () {
    $("#DOB").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#EmergencyContactDOB").datepicker({ dateFormat: 'dd-mm-yy' });
    if ($("#strArrLst").val() != "") {
        UpdateRepresenterTable($("#strArrLst").val());
        Edit_A();
    }

    $("#chkBillingSameAsAbove").click(function () {
        var IsChecked = $("#chkBillingSameAsAbove").is(":checked")
        debugger;
        if (IsChecked == true) {
            $("#BillingStreetAddress").val($("#StreetAddress").val());
            $("#BillingSuburb").val($("#Suburb").val());
            $("#BillingState").val($("#State option:selected").val());
            $("#BillingPostalCode").val($("#PostalCode").val());
        }
        else {
            $("#BillingStreetAddress").val('');
            $("#BillingSuburb").val('');
            $("#BillingState").val(0);
            $("#BillingPostalCode").val('');

        }
    });

    $("#btn_SavedRepItemsItems").click(function () {

        var ChekIfUpdate = $("#EditID").val();

        if (ChekIfUpdate == "") {
            Save();
            $("#EditID").val('');
        }
        else {
            update(ChekIfUpdate);
            $("#EditID").val('');
        }
        Edit_A();
    });


    function Edit_A() {
        $(".linkEdit").click(function () {
            var id = this.id.split('_')[1];
            debugger;
            $("#EditID").val(id);
            GetData(id);
        });
    }
    function linkDelete() {
        $(".linkDelete").click(function () {
            var id = this.id.split('_')[1];
            Delete(id);
        });
    }

    function GetData(ID) {
        var arrlist = $("#strArrLst").val();
        var arr = arrlist.split('æ');
        for (var i = 0; i < arr.length; i++) {
            if (i == ID) {
                var getData = arr[i].split('₧');
                $("#clsAuthRepItemsItems_RepresenterTypeId").val(getData[1]);
                $("#clsAuthRepItemsItems_RepresenterTitleId").val(getData[2]);
                $("#clsAuthRepItemsItems_RepresenterFirstName").val(getData[3]);
                $("#clsAuthRepItemsItems_RepresenterMiddleName").val(getData[4]);
                $("#clsAuthRepItemsItems_RepresenterLastName").val(getData[5]);
                $("#clsAuthRepItemsItems_RepresenterStreetAddress").val(getData[6]);
                $("#clsAuthRepItemsItems_RepresenterSuburb").val(getData[7]);
                $("#clsAuthRepItemsItems_RepresenterPostal").val(getData[8]);
                if (getData[9] == "True") {
                    $('#clsAuthRepItemsItems_RepresenterEvidenceOfAuthorityPro').prop('checked', true);
                }
                else {
                    $('#clsAuthRepItemsItems_RepresenterEvidenceOfAuthorityPro').prop('checked', false);
                }
            }
        }
    }

    function Delete(ID) {
        var finalResult = "";
        var arrlist = $("#strArrLst").val();
        var arr = arrlist.split('æ');
        for (var i = 0; i < arr.length; i++) {
            if (i != ID) {
                var Data = arr[i].split('₧');
                if (finalResult != "") { finalResult += "æ"; }
                var makesString = Data[0] + "₧" + Data[1] + "₧" + Data[2] + "₧" + Data[3] + "₧" + Data[4] + "₧" + Data[5]
               + "₧" + Data[6] + "₧" + Data[7] + "₧" + Data[8] + "₧" + Data[9];
                finalResult += makesString;

            }
        }
        $("#strArrLst").val(finalResult);
        UpdateRepresenterTable(finalResult);
        ClearScreen();
    }

    function update(ID) {
        var finalResult = "";
        var arrlist = $("#strArrLst").val();
        var arr = arrlist.split('æ');
        for (var i = 0; i < arr.length; i++) {
            if (i == ID) {
                var _id = ID;
                var _retrType = $("#clsAuthRepItemsItems_RepresenterTypeId").val();
                var _retrTitle = $("#clsAuthRepItemsItems_RepresenterTitleId").val();
                var _firstName = $("#clsAuthRepItemsItems_RepresenterFirstName").val();
                var _middleName = $("#clsAuthRepItemsItems_RepresenterMiddleName").val();
                var _lastname = $("#clsAuthRepItemsItems_RepresenterLastName").val();
                var _streetAddress = $("#clsAuthRepItemsItems_RepresenterStreetAddress").val();
                var _suburb = $("#clsAuthRepItemsItems_RepresenterSuburb").val();
                var _postal = $("#clsAuthRepItemsItems_RepresenterPostal").val();
                var _eviOfAut = "False";
                if ($("#clsAuthRepItemsItems_RepresenterEvidenceOfAuthorityPro").is(":checked")) {
                    _eviOfAut = "True";
                }
                if (finalResult != "") { finalResult += "æ"; }
                var makesString = _id + "₧" + _retrType + "₧" + _retrTitle + "₧" + _firstName + "₧" + _middleName + "₧" + _lastname
                 + "₧" + _streetAddress + "₧" + _suburb + "₧" + _postal + "₧" + _eviOfAut;
                finalResult += makesString;
            }
            else {
                var Data = arr[i].split('₧');
                if (finalResult != "") { finalResult += "æ"; }
                var makesString = Data[0] + "₧" + Data[1] + "₧" + Data[2] + "₧" + Data[3] + "₧" + Data[4] + "₧" + Data[5]
               + "₧" + Data[6] + "₧" + Data[7] + "₧" + Data[8] + "₧" + Data[9];
                finalResult += makesString;
            }

        }

        $("#strArrLst").val(finalResult);
        UpdateRepresenterTable(finalResult);
        ClearScreen();
    }

    function Save() {
        var arrlist = $("#strArrLst").val();
        var _id;
        if (arrlist == "") {
            _id = 0;
        }
        else {
            _id = arrlist.split('æ').length;
        }
        var _retrType = $("#clsAuthRepItemsItems_RepresenterTypeId").val();
        var _retrTitle = $("#clsAuthRepItemsItems_RepresenterTitleId").val();
        var _firstName = $("#clsAuthRepItemsItems_RepresenterFirstName").val();
        var _middleName = $("#clsAuthRepItemsItems_RepresenterMiddleName").val();
        var _lastname = $("#clsAuthRepItemsItems_RepresenterLastName").val();
        var _streetAddress = $("#clsAuthRepItemsItems_RepresenterStreetAddress").val();
        var _suburb = $("#clsAuthRepItemsItems_RepresenterSuburb").val();
        var _postal = $("#clsAuthRepItemsItems_RepresenterPostal").val();
        var _eviOfAut = "False";
        if ($("#clsAuthRepItemsItems_RepresenterEvidenceOfAuthorityPro").is(":checked")) {
            _eviOfAut = "True";
        }

        if (arrlist == "") {

            var makesString = _id + "₧" + _retrType + "₧" + _retrTitle + "₧" + _firstName + "₧" + _middleName + "₧" + _lastname
           + "₧" + _streetAddress + "₧" + _suburb + "₧" + _postal + "₧" + _eviOfAut;
            arrlist = makesString;
        }
        else {
            arrlist += "æ";

            var makesString = _id + "₧" + _retrType + "₧" + _retrTitle + "₧" + _firstName + "₧" + _middleName + "₧" + _lastname
            + "₧" + _streetAddress + "₧" + _suburb + "₧" + _postal + "₧" + _eviOfAut;
            arrlist += makesString;
        }
        $("#strArrLst").val(arrlist);
        UpdateRepresenterTable(arrlist);
        ClearScreen();
    }
    function ClearScreen() {
        $("#clsAuthRepItemsItems_RepresenterTypeId").val('0');
        $("#clsAuthRepItemsItems_RepresenterTitleId").val('0');
        $("#clsAuthRepItemsItems_RepresenterFirstName").val('');
        $("#clsAuthRepItemsItems_RepresenterMiddleName").val('');
        $("#clsAuthRepItemsItems_RepresenterLastName").val('');
        $("#clsAuthRepItemsItems_RepresenterStreetAddress").val('');
        $("#clsAuthRepItemsItems_RepresenterSuburb").val('');
        $("#clsAuthRepItemsItems_RepresenterPostal").val('');
        $('#clsAuthRepItemsItems_RepresenterEvidenceOfAuthorityPro').prop('checked', false);
        $("#EditID").val('');
    }
    function UpdateRepresenterTable(str) {
        debugger;
        if (str != "") {
            var arr = str.split('æ');

            var htmlTable = "<table>";
            for (var i = 0; i < arr.length; i++) {
                var model = arr[i];
                htmlTable += "<tr>";
                var obj = arr[i].split('₧');

                htmlTable += "<td>";
                htmlTable += obj[3] + " " + obj[4];
                htmlTable += "</td>";
                htmlTable += "<td>";
                htmlTable += " <a href='javascript:void()' class='linkEdit' id='a_" + obj[0] + "'><span class='glyphicon glyphicon-pencil edit-icon' aria-hidden='true'></span></a> ";
                htmlTable += " <a href='javascript:void()' class='linkDelete' id='a_" + obj[0] + "'><span class='glyphicon glyphicon-trash del-icon' aria-hidden='true'></span></a>";
                htmlTable += "</td>";


                htmlTable += "</tr>";
            }
            htmlTable += "</table>";
            $("#tbl_Representer").html(htmlTable);
            linkDelete();
        }
        else {
            $("#tbl_Representer").html("");
        }
    }
    // Setup form validation on the #register-form element
    $("#frmresidancy").validate({

        // Specify the validation rules
        rules: {

            //Care Recipient
            TitleId: { selectcheck: true },
            FirstName: "required",
            MiddleName: "required",
            LastName: "required",
            DOB: "required",
            MaritalStatusID: { selectcheck: true },

            //Current Residential Address
            StreetAddress: "required",
            Suburb: "required",
            PostalCode: "required",
            State: { selectcheck: true },

            //Billing Address
            BillingStreetAddress: "required",
            BillingSuburb: "required",
            BillingPostalCode: "required",
            BillingState: { selectcheck: true },

            //Emergency Contact
            EmergencyContactMiddleName: "required",
            EmergencyRelationShipID: { selectcheck: true },
            EmergencyContactTitleID: { selectcheck: true },
            EmergencyContactFirstName: "required",
            EmergencyContactState: { selectcheck: true },
            EmergencyContactLastName: "required",
            EmergencyContactLastName: "required",
            EmergencyContactDOB: "required",
            EmergencyContactStreetAddress: "required",
            EmergencyContactSuburb: "required",
            EmergencyContactPostalCode: "required",
            BillingCName: "required",
            EmergencyContactHomePhone: "required"


            //strPassowrd: "required",
            //emailAddress: {
            //    required: true,
            //    email: true
            //}
        },



        // Specify the validation error messages
        messages: {
            //Care Recipient
            TitleId: "please select title",
            FirstName: "please enter your first name",
            MiddleName: "please enter middle name",
            LastName: "please enter last name",
            DOB: "please enter DOB",
            MaritalStatusID: "please select marital status",

            //Current Residential Address
            StreetAddress: "please enter street address",
            Suburb: "please enter suburb",
            PostalCode: "please enter postcode",
            State: "please select state",

            //Billing Address
            BillingCName:"please enter c-/ name",
            BillingStreetAddress: "please enter street address",
            BillingSuburb: "please enter suburb",
            BillingPostalCode: "Please enter postcode",
            BillingState: "please select State",

            //Emergency Contact
            EmergencyRelationShipID: "Please select relationship",
            EmergencyContactTitleID: "Please select title",
            EmergencyContactFirstName: "Please enter first name",
            EmergencyContactLastName: "Please enter last name",           
            EmergencyContactDOB: "Please enter DOB",
            EmergencyContactStreetAddress: "Please enter street address",
            EmergencyContactSuburb: "Please enter suburb",
            EmergencyContactPostalCode: "Please enter postcode",
            EmergencyContactState: "Please select state",
            EmergencyContactMiddleName: "Please enter middle name",
            EmergencyContactHomePhone: "Please enter home phone number"
           
        },

        submitHandler: function (form) {
            form.submit();
        }
    });


    jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '0');
    }, "year required");
});