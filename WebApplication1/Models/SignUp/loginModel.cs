﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class loginModel
    {
        [Required(ErrorMessage = "Please enter username.")]
        [DisplayName("User Name:")]
        public string userName { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        [DisplayName("Password:")]
        public string password { get; set; }

        public string emailAddress { get; set; }

        [DisplayName("Number of Facilities:")]
        public int noOfFacility { get; set; }

        public int userTypeID { get; set; }

        public DateTime memberShipExpiryDate { get; set; }

        public int customerAddressID { get; set; }

        public int pictureID { get; set; }

        public int isActive { get; set; }

        public int isDeleted { get; set; }

        public string msg { get; set; }
    }
}