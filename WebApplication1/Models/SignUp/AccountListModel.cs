﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class AccountListModel
    {
        public int id { get; set; }

        public string userName { get; set; }

        public string password { get; set; }

        public string emailAddress { get; set; }

        public bool isActive { get; set; }

        public bool isDeleted { get; set; }

        public DateTime memberShipExpiryDate { get; set; }

    }
}