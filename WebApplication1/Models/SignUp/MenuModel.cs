﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models.FacilitiesForms;

namespace WebApplication1.Models
{
    public class MenuModel
    {
        /// <summary>
        /// userID
        /// </summary>
        public int userID { get; set; }

        /// <summary>
        /// user Role
        /// </summary>
        public int userRole { get; set; }


        /// <summary>
        /// facility ID
        /// </summary>
        public int facilityID { get; set; }


        /// <summary>
        /// facility Name
        /// </summary>
        public string facilityName { get; set; }

        /// <summary>
        /// Image URL
        /// </summary>
        public string ImageURL { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string adress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Contact { get; set; }

    }
}