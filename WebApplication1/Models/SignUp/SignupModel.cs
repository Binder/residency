﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class SignupModel
    {
        [Required(ErrorMessage = "Please enter username.")]
        [DisplayName("User Name:")]
        public string strUserName { get; set; }

        [DisplayName("Password:")]
        public string strPassowrd { get; set; }

        [Required(ErrorMessage = "Please enter email.")]
        [DisplayName("Email:")]
        public string emailAddress { get; set; }

        [DisplayName("Number of Facilities:")]
        public int noOfFacility { get; set; }

        public int userTypeID { get; set; }

        public DateTime memberShipExpiryDate { get; set; }

        [DisplayName("Is Active:")]
        public bool isActive { get; set; }

        public bool isDeleted { get; set; }

        public string msg { get; set; }

        public int id { get; set; }
    }
}