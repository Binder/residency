﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models.SaveDocumets
{
    /// <summary>
    /// 
    /// </summary>
    public class AccommodationPaymentModel
    {
        public AccommodationPaymentModel()
        {
            PaymentMethods =new  List<SelectListItem>();
            DrawdownRecoveryMethods = new List<SelectListItem>();
        }


        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Document ID
        /// </summary>
        public int DocumentID { get; set; }

        /// <summary>
        /// Accommodation Payement
        /// </summary>
        [DisplayName("Accommodation Payement:")]
        public bool AccomPayment { get; set; }

        /// <summary>
        /// Accommodation Contribution
        /// </summary>
        [DisplayName("Accommodation Contribution:")]
        public bool AccomContribution { get; set; }

        /// <summary>
        /// RAD Equivalent
        /// </summary>
        [DisplayName("RAD Equivalent:")]
        public Nullable<decimal> RADEquivalent { get; set; }

        /// <summary>
        /// DAP Equivalent
        /// </summary>
        [DisplayName("DAP Equivalent:")]
        public Nullable<decimal> DAPEquivalent { get; set; }

        /// <summary>
        /// MPIR
        /// </summary>
        [DisplayName("MPIR:")]
        public Nullable<int> MPIR { get; set; }

        /// <summary>
        /// Admissions Date
        /// </summary>
        [DisplayName("Admissions Date:")]
        public DateTime AdmissionsDate { get; set; }
        /// <summary>
        /// Admissions Date
        /// </summary>
        [DisplayName("Admissions Date:")]
        public string AdmissionsDate1 { get; set; }


        /// <summary>
        /// Project Payment Decision Date
        /// </summary>
        [DisplayName("Project Payment Decision Date:")]
        public DateTime ProjectPaymentDecDate { get; set; }
        /// <summary>
        /// Project Payment Decision Date
        /// </summary>
        [DisplayName("Project Payment Decision Date:")]
        public string ProjectPaymentDecDate1 { get; set; }

        /// <summary>
        /// Actual Payment Decision Date
        /// </summary>
        [DisplayName("Actual Payment Decision Date:")]
        public DateTime ActualPaymentDate { get; set; }

        /// <summary>
        /// Actual Payment Decision Date
        /// </summary>
        [DisplayName("Actual Payment Decision Date:")]
        public string ActualPaymentDate1 { get; set; }

        /// <summary>
        /// Payment Method
        /// </summary>
        [DisplayName("Payment Method:")]
        public int PaymentMethod { get; set; }

        /// <summary>
        /// RAD/RAC Portion
        /// </summary>
        [DisplayName("RAD/RAC Portion:")]
        public Nullable<decimal> RAD { get; set; }

        /// <summary>
        /// DAP/DAC Portion
        /// </summary>
        [DisplayName("DAP/DAC Portion:")]
        public Nullable<decimal> DAP { get; set; }

        /// <summary>
        /// Drawdown DAP From RAD
        /// </summary>
        [DisplayName("Drawdown DAP From RAD:")]
        public bool DrawdownDAP { get; set; }

        /// <summary>
        /// Drawdown Recovery Method
        /// </summary>
        [DisplayName("Drawdown Recovery Method:")]
        public int DrawdownRecoMethod { get; set; }


        public IList<SelectListItem> PaymentMethods { get; set; }
        public IList<SelectListItem> DrawdownRecoveryMethods { get; set; }
    }
}