﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.SaveDocumets
{
    public class IncomeAndAssetsModel
    {
        public int ID { get; set; }
        public int DocumentID { get; set; }

        [DisplayName("Care Recipient Declined to Declare:")]
        public bool DeclineToDeclare { get; set; }

        [DisplayName("Government Income Support Payments:")]
        public decimal GovtIncomeSuppPayment { get; set; }

        [DisplayName("Other Income:")]
        public decimal OtherIncome { get; set; }

        [DisplayName("Care Recipient's Home:")]
        public decimal YourIncome { get; set; }

        [DisplayName("Accommodation Bond:")]
        public decimal AccommodationBond { get; set; }

        [DisplayName("Net Retirement Village Entry Contribution:")]
        public decimal NetRetirementVillageEntCont { get; set; }

        [DisplayName("Financial Accounts:")]
        public decimal FinancialAccounts { get; set; }

        [DisplayName("Shares:")]
        public decimal Shares { get; set; }

        [DisplayName("Managed Investments:")]
        public decimal ManagedInvestment { get; set; }

        [DisplayName("Assessable Income Streams:")]
        public decimal AssessableIncomeStreams { get; set; }

        [DisplayName("Foregin Assets:")]
        public decimal ForeginAssets { get; set; }

        [DisplayName("Real Estate and Business Interests:")]
        public decimal RealEstateAndBusinessInt { get; set; }

        [DisplayName("Private Trusts and Private Companies:")]
        public decimal PrivateTrusts { get; set; }

        [DisplayName("Gifts/Deprivation:")]
        public decimal GiftsOrDeprivation { get; set; }

        [DisplayName("OtherAssets:")]
        public decimal OtherAssets { get; set; }

        [DisplayName("Full Pension:")]
        public bool FullPension { get; set; }

        [DisplayName("Part Pension:")]
        public bool PartPension { get; set; }

        [DisplayName("Self-funded retiree:")]
        public bool SelfFundedRetiree { get; set; }

        [DisplayName("Personal Loans:")]
        public decimal DebtsPersonalLoans { get; set; }

        [DisplayName("Other Debts:")]
        public decimal DebtsOther { get; set; }

        [DisplayName("Prisoner of War Compensation:")]
        public decimal AsstRedAmtPrisonerOfWar { get; set; }

        [DisplayName("Please tick if respite care:")]
        public bool RerspiteCareOnly { get; set; }

        [DisplayName("Sub total of Assets:")]
        public double NetAssets { get; set; }

        [DisplayName("Sub total of Debts:")]
        public double NetDebts { get; set; }

        [DisplayName("Net Assets after debts:")]
        public double NetIncome { get; set; }

        [DisplayName("Pension Number:")]
        public int PensionNumber { get; set; }

        //public double TotalOfIncomeAssets { get; set; }
        //public double TotalOfDebts { get; set; }
        //public double TotalOfNetAssetsAfterDebts { get; set; }
    }
}