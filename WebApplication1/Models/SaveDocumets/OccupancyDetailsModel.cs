﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models.SaveDocumets
{
    public class OccupancyDetailsModel
    {


        public OccupancyDetailsModel()
        {
            PreEntryLeaves=new List<SelectListItem>();


        }
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Document Id
        /// </summary>
        public int DocumentId { get; set; }

        //Facility Details
        [DisplayName("Name:")]
        public string FacilityName { get; set; }

        [DisplayName("Adress:")]
        public string FacilityAdress { get; set; }

        [DisplayName("Suburb:")]
        public string FacilitySuburb { get; set; }

        [DisplayName("State:")]
        public string FacilityState { get; set; }

        [DisplayName("Postal Code:")]
        public string FacilityPostalCode { get; set; }

        //Provider Name
        [DisplayName("Company Name:")]
        public string ProviderName { get; set; }

        [DisplayName("Adress:")]
        public string ProviderAdress { get; set; }

        [DisplayName("Suburb:")]
        public string ProviderSuburb { get; set; }

        [DisplayName("State:")]
        public string ProviderState { get; set; }

        [DisplayName("Postal Code:")]
        public string ProviderPostalCode { get; set; }

        [DisplayName("ABN:")]
        public string ProviderABN { get; set; }


        [DisplayName("Room:")]
        public string Wing { get; set; }

        [DisplayName("Agreed Entry Date:")]
        public string AgreedEntryDate { get; set; }

        [DisplayName("Period of Pre-Entry Leave (if applicable):")]
        public string PeriodOfPreEntryFrom { get; set; }

        [DisplayName("To:")]
        public string PeriodOfPreEntryTo { get; set; }

        [DisplayName("Tansferred from residential care or home care?:")]
        public bool Transferred { get; set; }

        [DisplayName("Date Care and Services Commenced with Other Provider:")]
        public string DateCareServiceCommmenced { get; set; }

        [DisplayName("Date Care and Services Ceased with Other Provider:")]
        public string DateCareServiceCeased { get; set; }

        [DisplayName("Pre-Entry Leave: ")]
        public bool PreEntryLeave { get; set; }

        public IList<SelectListItem> PreEntryLeaves { get; set; }



    }

    public enum PreEntryLeaves
    {
        Yes = 1,
        No = 0
    }
}