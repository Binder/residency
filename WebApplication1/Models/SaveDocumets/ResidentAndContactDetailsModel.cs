﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models.SaveDocumets
{
    public class ResidentAndContactDetailsModel
    {

        public ResidentAndContactDetailsModel()
        {
            AdressStates = new List<SelectListItem>();
            BillingStates = new List<SelectListItem>();
            EmergancyStates = new List<SelectListItem>();
            Athority1States = new List<SelectListItem>();
            Athority2States = new List<SelectListItem>();
            TitleOptions = new List<SelectListItem>();
            MaritalStatus = new List<SelectListItem>();
            EmergencyRelationship = new List<SelectListItem>();
            EmergencyContactTitle = new List<SelectListItem>();
            LstAuthRepItemsItems = new List<clsAuthRepItems>();
            clsAuthRepItemsItems = new clsAuthRepItems();
        }

        public int DocumentID { get; set; }

        public int DocPerDetid { get; set; }
        //you/Care receipt
        [DisplayName("Title:")]
        public int TitleId { get; set; }

        [DisplayName("First Name:")]
        public string FirstName { get; set; }

        [DisplayName("Middle Name:")]
        public string MiddleName { get; set; }

        [DisplayName("Last Name:")]
        public string LastName { get; set; }

        [DisplayName("Date Of Birth:")]
        public string DOB { get; set; }

        [DisplayName("Marital Status:")]
        public int MaritalStatusID { get; set; }

        //Address
     
        [DisplayName("Street Address:")]
        public string StreetAddress { get; set; }

        [DisplayName("Suburb:")]
        public string Suburb { get; set; }

        [DisplayName("State:")]
        public int State { get; set; }

        [DisplayName("PostCode:")]
        public string PostalCode { get; set; }

        //Billing Address

        [DisplayName("C/- (Name):")]
        public string BillingCName { get; set; }

        [DisplayName("Street Address:")]
        public string BillingStreetAddress { get; set; }

        [DisplayName("Suburb:")]
        public string BillingSuburb { get; set; }

        [DisplayName("State:")]
        public int BillingState { get; set; }

        [DisplayName("Postcode:")]
        public string BillingPostalCode { get; set; }

        [DisplayName("Private and Confidential:")]
        public bool PrivateAndConfidential { get; set; }


        //Emergency Contact

        [DisplayName("Relationship:")]
        public int EmergencyRelationShipID { get; set; }

        [DisplayName("Authorised Representative?:")]
        public bool EmergencyAuthorisedRepresentative { get; set; }

        [DisplayName("Title:")]
        public int EmergencyContactTitleID { get; set; }

        [DisplayName("First Name:")]
        public string EmergencyContactFirstName { get; set; }

        [DisplayName("Middle Name:")]
        public string EmergencyContactMiddleName { get; set; }

        [DisplayName("Last Name:")]
        public string EmergencyContactLastName { get; set; }

        [DisplayName("Date Of Birth:")]
        public string EmergencyContactDOB { get; set; }

        [DisplayName("Street Address:")]
        public string EmergencyContactStreetAddress { get; set; }

        [DisplayName("Suburb:")]
        public string EmergencyContactSuburb { get; set; }

        [DisplayName("State:")]
        public int EmergencyContactState { get; set; }

        [DisplayName("PostCode:")]
        public string EmergencyContactPostalCode { get; set; }

        [DisplayName("Home Phone Number:")]
        public string EmergencyContactHomePhone { get; set; }

        [DisplayName("Mobile Number:")]
        public string EmergencyContactMobileNumber{ get; set; }


        public string strArrLst { get; set; }

        public IList<SelectListItem> AdressStates { get; set; }
        public IList<SelectListItem> BillingStates { get; set; }
        public IList<SelectListItem> EmergancyStates { get; set; }
        public IList<SelectListItem> Athority1States { get; set; }
        public IList<SelectListItem> Athority2States { get; set; }
        public IList<SelectListItem> TitleOptions { get; set; }
        public IList<SelectListItem> MaritalStatus { get; set; }
        public IList<SelectListItem> EmergencyRelationship { get; set; }
        public IList<SelectListItem> EmergencyContactTitle { get; set; }

        public clsAuthRepItems clsAuthRepItemsItems { get; set; }

        public List<clsAuthRepItems> LstAuthRepItemsItems { get; set; }
    }

    public class clsAuthRepItems
    {

        public clsAuthRepItems()
        {
            RepresenterTypes = new List<SelectListItem>(); 
            RepresenterTitle = new List<SelectListItem>();
            RepresenterStates = new List<SelectListItem>();
        }

        public int ID { get; set; }

        [DisplayName("Document ID:")]
        public int DocumentID { get; set; }

        [DisplayName("Representative Type:")]
        public int RepresenterTypeId { get; set; }

        [DisplayName("Title:")]
        public int RepresenterTitleId { get; set; }

        [DisplayName("First Name:")]
        public string RepresenterFirstName { get; set; }

        [DisplayName("Middle Name:")]
        public string RepresenterMiddleName { get; set; }

        [DisplayName("Last Name:")]
        public string RepresenterLastName { get; set; }

        [DisplayName("Street Address:")]
        public string RepresenterStreetAddress { get; set; }

        [DisplayName("Suburb:")]
        public string RepresenterSuburb { get; set; }

        [DisplayName("State:")]
        public int RepresenterState { get; set; }

        [DisplayName("Postcode:")]
        public string RepresenterPostal { get; set; }

        [DisplayName("Evidence Of Authority Provided?")]
        public bool RepresenterEvidenceOfAuthorityPro { get; set; }

      
        //List
        public IList<SelectListItem> RepresenterTypes { get; set; }
        public IList<SelectListItem> RepresenterTitle { get; set; }
        public IList<SelectListItem> RepresenterStates { get; set; }


    }

}