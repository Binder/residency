﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class FacilitiesSettingsModel
    {

        public FacilitiesSettingsModel()
        {
            AvailableStates = new List<SelectListItem>();
        }        

        public int FacilitiesSettingID { get; set; }

        [DisplayName("Company Name:")]
        public string companyName { get; set; }

        [DisplayName("Account Manager:")]
        public string AccountManager { get; set; }

        [DisplayName("Phone Contact:")]
        public string strPhoneContact { get; set; }

        [DisplayName("Email Contact:")]
        public string strEmailContact { get; set; }

        [DisplayName("Membership Expiry:")]
        public string membershipExpiry { get; set; }

        [DisplayName("Address:")]
        public string strAddress { get; set; }

        [DisplayName("Postal Code:")]
        public string strPostalCode { get; set; }

        [DisplayName("State:")]
        public int stateID { get; set; }

        [DisplayName("ABN:")]
        public string ABN { get; set; }

        [DisplayName("Suburb:")]
        public string suburb { get; set; }

        public IList<SelectListItem> AvailableStates { get; set; }

        [DisplayName("Country:")]
        public string strCountry { get; set; }

        public int intPictureID { get; set; }

        public string registerUsers { get; set; }

        public string msg { get; set; }

        public int FacilitiesNo { get; set; }

        public int userID { get; set; }

        public string FacilityName { get; set; }

        public bool canEdit { get; set; }

        [DisplayName("Logo:")]
        public int pictureID { get; set; }

        public string picturePath { get; set; }

    }
}