﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class FacilitiesOwnerSettingsModel
    {
        public FacilitiesOwnerSettingsModel()
        {
            AvailableStates = new List<SelectListItem>();
        }
        [DisplayName("User Name:")]
        public string userName { get; set; }
        [DisplayName("Password:")]
        public string strPassowrd { get; set; }
        [DisplayName("Account Manager:")]
        public string accountManager { get; set; }
        [DisplayName("Provider Name:")]
        public string providerName { get; set; }
        [DisplayName("Phone Contact:")]
        public string strPhoneContact { get; set; }
        [DisplayName("Email Contact:")]
        public string strEmailContact { get; set; }
        [DisplayName("Membership Expiry:")]
        public string membershipExpiry { get; set; }
        [DisplayName("Address:")]
        public string strAddress { get; set; }
        [DisplayName("Postal Code:")]
        public string strPostalCode { get; set; }
        [DisplayName("State:")]
        public int stateID { get; set; }
        [DisplayName("Suburb:")]
        public string suburb { get; set; }

        public IList<SelectListItem> AvailableStates { get; set; }
        [DisplayName("Country:")]
        public string strCountry { get; set; }
        public string msg { get; set; }
        public int FacilitiesID { get; set; }
        public bool canEdit { get; set; }
    }
}