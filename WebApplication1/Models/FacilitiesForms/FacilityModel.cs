﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.FacilitiesForms
{
    public class FacilityModel
    {
        public int id { get; set; }

        public int userID { get; set; }

        [Required(ErrorMessage = "Please enetr facility name.")]
        [DisplayName("Facility Name:")]
        public string FacilityName  { get; set; }
    }
}