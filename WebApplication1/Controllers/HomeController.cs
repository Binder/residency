﻿using Residency.Data;
using Residency.Services;
using Residency.Services.FacilitiesForms;
using Residency.Services.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.App_Start;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public partial class HomeController : Controller
    {
        #region Feilds

        private readonly ILoginService _LoginService;
        private readonly IFacilitiesSettingService _FacilitiesSetting;
        private readonly IDocumentService _DocumentPersoanlInfo;


        #endregion

        #region constructor

        public HomeController()
        {
            this._LoginService = DependencyResolver.Current.GetService<LoginService>();
            this._FacilitiesSetting = DependencyResolver.Current.GetService<FacilitiesSettingService>();
            this._DocumentPersoanlInfo = DependencyResolver.Current.GetService<DocumentService>();
        }


        #endregion

        #region Login

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            loginModel obj = new loginModel();
            return View(obj);
        }

        /// <summary>
        /// Http Post
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(loginModel obj)
        {
            if (!ModelState.IsValid)
                return View(obj);
            try
            {
                User isValid = _LoginService.getUserIDByUserName(obj.userName, obj.password);
                if (isValid != null)
                {
                    if (isValid.memberShipExpiryDate != null && DateTime.Now < isValid.memberShipExpiryDate)
                    {
                        Session["userID"] = isValid.ID;
                        Session["UserName"] = isValid.userName;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        obj.msg = string.Format("Your membership is expried.");
                    }
                }
                else
                {
                    obj.msg = "Please enter valid credentials";
                }
            }
            catch (Exception ex)
            {
                Common.EnterLog(ex.Message, Convert.ToInt32(Session["UserID"]));
            }
            return View(obj);
        }

        #endregion

        #region Home
        [LogActionFilter]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public ActionResult LoginAsSelectedUSerID(int userID)
        {
            if (userID > 0)
            {
                User objLogin = _LoginService.getAccountUserByID(userID);
                if (objLogin != null)
                {
                    Session["SuperAdminID"] = Convert.ToInt32(Session["userID"]);
                    Session["userID"] = objLogin.ID;
                    Session["UserName"] = objLogin.userName;
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("");
        }

        /// <summary>
        /// Sign in As Lowyer
        /// </summary>
        /// <returns></returns>
        public ActionResult SignInAsAdmin()
        {
            if (Convert.ToInt32(Session["SuperAdminID"]) > 0)
            {
                User objLogin = _LoginService.getAccountUserByID(Convert.ToInt32(Session["SuperAdminID"]));
                if (objLogin != null)
                {
                    Session["userID"] = objLogin.ID;
                    Session["UserName"] = objLogin.userName;
                    Session["SuperAdminID"] = null;
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("");
        }

        #endregion

        #region Sign Up


        [LogActionFilter]
        public ActionResult AccountCreationList()
        {
            AccountListModel obj = new AccountListModel();
            return View(obj);
        }


        [HttpGet]
        [LogActionFilter]
        public JsonResult getAccounts()
        {
            List<AccountListModel> objList = new List<AccountListModel>();

            var oldModel = _LoginService.getAllUsers(Convert.ToInt32(Common.UserType.Admin));
            foreach (User item in oldModel)
            {
                AccountListModel obj = new AccountListModel();
                obj.userName = item.userName;
                obj.emailAddress = item.emailAddress;
                obj.isActive = Convert.ToBoolean(item.isAcive);
                obj.memberShipExpiryDate = Convert.ToDateTime(item.memberShipExpiryDate);
                obj.isDeleted = Convert.ToBoolean(item.isDelete);
                obj.id = item.ID;
                objList.Add(obj);
            }
            return Json(objList, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Sign Up
        /// </summary>
        /// <returns></returns>
        [LogActionFilter]
        public ActionResult AccountCreation(int? ID)
        {
            SignupModel obj = new SignupModel();
            if (ID != null && ID > 0)
            {
                User getObj = _LoginService.getAccountUserByID(Convert.ToInt32(ID));
                obj = new SignupModel();
                obj.strUserName = getObj.userName;
                obj.noOfFacility = (string.IsNullOrEmpty(Convert.ToString(getObj.noOfFacilities)) == true ? 0 : Convert.ToInt32(getObj.noOfFacilities));
                obj.emailAddress = getObj.emailAddress;
                obj.isActive = Convert.ToBoolean(getObj.isAcive);
                obj.memberShipExpiryDate = Convert.ToDateTime(getObj.memberShipExpiryDate);
                obj.isDeleted = Convert.ToBoolean(getObj.isDelete);
                obj.id = getObj.ID;
                ViewBag.TitleName = "Edit Account";
            }
            else
            {
                obj.strPassowrd = Convert.ToString(Common.RandomString(10));
                obj.isActive = true;
                ViewBag.TitleName = "Add Account";
            }
            ViewBag.PreURL = "/Home/AccountCreationList";

            return View(obj);
        }



        [LogActionFilter]
        [HttpGet]

        public string updatePassword(string ID, string strpassword)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(ID))
            {
                try
                {
                    User objModel = _LoginService.getAccountUserByID(Convert.ToInt32(ID));
                    objModel.password = strpassword;
                    _LoginService.UpdateUser(objModel);
                    return result = "1";
                }
                catch (Exception ex)
                {
                    Common.EnterLog(ex.Message, Convert.ToInt32(Session["UserID"]));
                }

            }
            return result;
        }


        /// <summary>
        /// Delete User Account
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [LogActionFilter]
        public ActionResult DeleteUserAccount(int? ID)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ID)))
            {
                User objModel = _LoginService.getAccountUserByID(Convert.ToInt32(ID));
                objModel.isDelete = true;
                _LoginService.UpdateUser(objModel);
            }
            return RedirectToAction("AccountCreationList");
        }


        /// <summary>
        /// Loywer Sign Up
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [LogActionFilter]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AccountCreation(SignupModel obj)
        {
            if (!ModelState.IsValid)
                return View(obj);
            if (obj.id > 0)//Update
            {
                User objModel = _LoginService.getAccountUserByID(obj.id);
                objModel.ID = obj.id;
                objModel.emailAddress = obj.emailAddress;
                objModel.noOfFacilities = obj.noOfFacility;
                objModel.isAcive = obj.isActive;//By default Active is TRUE
                if (_LoginService.UpdateUser(objModel))
                {
                    //do something
                }

                return RedirectToAction("AccountCreationList");
            }

            else//Insert New
            {
                if (!_LoginService.IsUserExist(obj.strUserName))
                {
                    try
                    {
                        obj.memberShipExpiryDate = DateTime.Now.AddYears(1);
                        //Fill model
                        User objModel = new User();
                        objModel.emailAddress = obj.emailAddress;
                        objModel.noOfFacilities = obj.noOfFacility;
                        objModel.password = obj.strPassowrd;
                        objModel.userName = obj.strUserName;
                        objModel.memberShipExpiryDate = obj.memberShipExpiryDate;
                        objModel.userTypeID = Convert.ToInt32(Common.UserType.Admin);
                        objModel.isAcive = obj.isActive;//By default Active is TRUE
                        objModel.isDelete = false;
                        if (_LoginService.saveUsers(objModel))
                        {
                            //Create Facility 
                            for (int i = 0; i < objModel.noOfFacilities; i++)
                            {
                                Facility objFacility = new Facility();
                                objFacility.facilityName = string.Format("Facility Setting {0}", (i + 1));
                                objFacility.userID = objModel.ID;
                                _LoginService.saveFacility(objFacility);
                            }

                            //Send email
                            string getSuperAdminEmailID = _LoginService.getSuperAdminEmailId();
                            string toEmail = objModel.emailAddress;
                            string subject = Common.getResourceValue("%RegisterEmail.Subject%");

                            string getHtml = System.IO.File.ReadAllText(Server.MapPath(@"\EmailTemplate\RegisterEmail.html"));
                            getHtml = getHtml.Replace("%userName%", objModel.userName);
                            getHtml = getHtml.Replace("%Password%", objModel.password);
                            getHtml = getHtml.Replace("%LiveSiteURL%", Common.getResourceValue("%LiveSiteURL%"));

                            Common.sendEmail(toEmail, subject, getHtml, "", getSuperAdminEmailID);

                        }
                        return RedirectToAction("AccountCreationList");
                    }
                    catch (Exception ex)
                    {
                        Common.EnterLog(ex.Message, Convert.ToInt32(Session["UserID"]));
                    }
                }
                else
                {
                    obj.msg = "User name already exists";
                }
            }
            return View(obj);
        }

        #endregion

        #region Logout
        [LogActionFilter]
        public ActionResult logout()
        {
            Session.Abandon();
            return RedirectToAction("Login");
        }

        #endregion

        #region Menu

        public string getMenu()
        {
            List<MenuModel> objList = new List<MenuModel>();
            if (Session["userID"] == null)
            {
                return RenderPartialViewToString("~/Views/Home/_MenuLayout.cshtml", null);
            }
            else
            {
                IList<facilityMenu> objFacilityLst = _LoginService.getFacilityByUserID(Convert.ToInt32(Session["userID"]));
                if (objFacilityLst.Count > 0)
                {
                    foreach (var item in objFacilityLst)
                    {
                        MenuModel obj = new MenuModel();
                        obj.facilityName = item.facilityName;
                        obj.userID = item.userID;
                        obj.userRole = item.userRole;
                        obj.facilityID = item.facilityID;
                        if (obj.userRole == Convert.ToInt32(Common.UserType.User))
                        {
                            Facility_Settings ObjF = _FacilitiesSetting.GetFacilitySettingByUser(obj.userID);
                            obj.ImageURL = ObjF.Picture.picturePath;
                            if (Request.QueryString["DocumentID"] != null)
                            {
                                DocumentPersonalDetail getObj = _DocumentPersoanlInfo.getPersonalDetailByDocumentID(Convert.ToInt32(Request.QueryString["DocumentID"]));
                                if (getObj != null)
                                {
                                    obj.Name = Convert.ToString(getObj.FirstName) + " " + Convert.ToString(getObj.MiddleName) + " " + Convert.ToString(getObj.LastName);
                                    obj.adress = getObj.Address;
                                    obj.Contact = Convert.ToString(getObj.EmergencyFirstName) + " " + Convert.ToString(getObj.EmergencyMiddleName) + " " + Convert.ToString(getObj.EmergencyLastName);
                                }
                            }
                        }
                        objList.Add(obj);
                    }
                    return RenderPartialViewToString("~/Views/Home/_MenuLayout.cshtml", objList);

                }
                else
                {
                    return RenderPartialViewToString("~/Views/Home/_MenuLayout.cshtml", null);
                }

            }
        }


        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion
    }
}