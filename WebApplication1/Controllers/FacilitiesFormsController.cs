﻿using Residency.Data;
using Residency.Services;
using Residency.Services.FacilitiesForms;
using Residency.Services.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.App_Start;
using WebApplication1.Models;
using WebApplication1.Models.FacilitiesForms;

namespace WebApplication1.Controllers
{
    public class FacilitiesFormsController : Controller
    {
        #region Feilds

        private readonly IFacilitiesOwnerSettingService _FacilitiesOwnerSetting;
        private readonly IFacilitiesSettingService _FacilitiesSetting;
        private readonly ILoginService _LoginService;



        #endregion

        #region constructor

        public FacilitiesFormsController()
        {
            this._FacilitiesOwnerSetting = DependencyResolver.Current.GetService<FacilitiesOwnerSettingService>();
            this._FacilitiesSetting = DependencyResolver.Current.GetService<FacilitiesSettingService>();
            this._LoginService = DependencyResolver.Current.GetService<LoginService>();
        }


        #endregion

        #region methods

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [LogActionFilter]
        public ActionResult AccountOwnerSetting()
        {
            FacilitiesOwnerSettingsModel obj = new FacilitiesOwnerSettingsModel();
            FacilitiesOwnerDetail getSetting = _FacilitiesOwnerSetting.GetOwnerSetting(Convert.ToInt32(Session["userID"]));
            if (getSetting != null)
            {
                obj.accountManager = getSetting.accountManager;
                obj.membershipExpiry = Convert.ToDateTime(getSetting.membershipExpiry).ToString(Common.getDateFormat());
                obj.strAddress = getSetting.strAddress;
                obj.strCountry = getSetting.strCountry;
                obj.strEmailContact = getSetting.strEmailContact;
                obj.strPassowrd = getSetting.strPassowrd;
                obj.strPhoneContact = getSetting.strPhoneContact;
                obj.strPostalCode = getSetting.strPostalCode;
                obj.stateID = getSetting.StateID;
                obj.userName = getSetting.userName;
                obj.FacilitiesID = getSetting.FacilitiesID;
                obj.providerName = getSetting.ProviderName;
                obj.suburb = getSetting.suburb;
            }
            else
            {
                obj.canEdit = true;
            }
            foreach (var c in _FacilitiesOwnerSetting.GetAllStates())
                obj.AvailableStates.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.stateID) });
            if (string.IsNullOrEmpty(obj.strCountry))
            {
                obj.strCountry = Common.getResourceValue("%DefaultCountry%");
            }
            return View(obj);
        }

        /// <summary>
        /// Account Owner Setting POST
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [LogActionFilter]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AccountOwnerSetting(FacilitiesOwnerSettingsModel obj)
        {
            if (!ModelState.IsValid)
                return View(obj);

            if (obj.FacilitiesID == 0)//insert
            {
                Facility_Owner_AccountSetting model = new Facility_Owner_AccountSetting();
                model.accountManager = obj.accountManager;
                model.emailContact = obj.strEmailContact;
                model.loginUserID = Convert.ToInt32(Session["userID"]);
                model.phoneContact = obj.strPhoneContact;
                model.strAddress = obj.strAddress;
                model.strCountry = obj.strCountry;
                model.strPostCode = obj.strPostalCode;
                model.StateID = obj.stateID;
                model.userName = obj.userName;
                model.ProviderName = obj.providerName;
                model.suburb = obj.suburb;
                _FacilitiesOwnerSetting.saveFacility_Owner_Account(model);
            }
            else//Update
            {
                Facility_Owner_AccountSetting model = _FacilitiesOwnerSetting.getOwnerSettingForUpdate(Convert.ToInt32(Session["userID"]));
                model.accountManager = obj.accountManager;
                model.emailContact = obj.strEmailContact;
                model.loginUserID = Convert.ToInt32(Session["userID"]);
                model.phoneContact = obj.strPhoneContact;
                model.strAddress = obj.strAddress;
                model.strCountry = obj.strCountry;
                model.strPostCode = obj.strPostalCode;
                model.StateID = obj.stateID;
                model.userName = obj.userName;
                model.ProviderName = obj.providerName;
                model.suburb = obj.suburb;
                _FacilitiesOwnerSetting.UpdateFacilitiesOwnerDetail(model);
            }
            return RedirectToAction("AccountOwnerSetting");
        }

        /// <summary>
        /// Facility Setting
        /// </summary>
        /// <param name="FacilityNo"></param>
        /// <returns></returns>
        [LogActionFilter]
        public ActionResult FacilitySetting(int FacilityID)

        {
            FacilitiesSettingsModel obj = new FacilitiesSettingsModel();
            if (FacilityID > 0)
            {
                Facility_Settings getModel = _FacilitiesSetting.GetFacilitySettingByUserID(Convert.ToInt32(Session["UserID"]), FacilityID);
                if (getModel != null)
                {
                    obj.AccountManager = getModel.accountManager;
                    obj.companyName = getModel.companyName;
                    obj.FacilitiesSettingID = getModel.id;
                    obj.intPictureID = Convert.ToInt32(getModel.pictureID);
                    if (getModel.membershipExpiry != null)
                    {
                        obj.membershipExpiry = Convert.ToDateTime(getModel.membershipExpiry).ToString(Common.getDateFormat());
                    }
                    obj.strAddress = getModel.strAddress;
                    obj.strCountry = getModel.strCountry;
                    obj.strEmailContact = getModel.emailContact;
                    obj.strPhoneContact = getModel.phoneContact;
                    obj.strPostalCode = getModel.postalAddress;
                    obj.stateID = Convert.ToInt32(getModel.stateID);
                    obj.ABN = getModel.ABN;
                    obj.suburb = getModel.suburb;
                    obj.picturePath = Convert.ToString(Convert.ToString(getModel.pictureID) == "" ? "" : getModel.Picture.picturePath);
                }
                else
                {

                    obj.canEdit = true;
                }
            }
            obj.userID = Convert.ToInt32(Session["UserID"]);
            obj.FacilitiesNo = FacilityID;
            obj.FacilityName = _FacilitiesSetting.getFacilityNameByID(FacilityID);
            obj.membershipExpiry = Convert.ToDateTime(_LoginService.getAccountUserByID(obj.userID).memberShipExpiryDate).ToString(Common.getDateFormat());
            foreach (var c in _FacilitiesOwnerSetting.GetAllStates())
                obj.AvailableStates.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.stateID) });
            if (string.IsNullOrEmpty(obj.strCountry))
            {
                obj.strCountry = Common.getResourceValue("%DefaultCountry%");
            }
            return View(obj);
        }


        [LogActionFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FacilitySetting(FacilitiesSettingsModel obj, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
                return View(obj);

            Picture objPicture = new Picture();
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    string fileName = (Common.RandomString(12) + "~" + file.FileName);
                    string path = Path.Combine(Server.MapPath("~/UploadedImages"), Path.GetFileName(fileName));
                    file.SaveAs(path);


                    objPicture.picturePath = "/UploadedImages/" + fileName;
                    _LoginService.savePicture(objPicture);

                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            }

            if (obj.FacilitiesSettingID == 0)//insert
            {
                Facility_Settings model = new Facility_Settings();
                model.accountManager = obj.AccountManager;
                model.emailContact = obj.strEmailContact;
                model.phoneContact = obj.strPhoneContact;
                model.strAddress = obj.strAddress;
                model.strCountry = obj.strCountry;
                model.postalAddress = obj.strPostalCode;
                model.stateID = obj.stateID;
                model.facilityID = obj.FacilitiesNo;
                model.companyName = obj.companyName;
                model.ABN = obj.ABN;
                model.suburb = obj.suburb;
                if (objPicture.id > 0)
                {
                    model.pictureID = objPicture.id;
                }
                _FacilitiesSetting.saveFacility(model);
            }
            else//uddate
            {
                Facility_Settings getModel = _FacilitiesSetting.GetFacilitySettingByUserID(Convert.ToInt32(Session["UserID"]), obj.FacilitiesNo);
                getModel.accountManager = obj.AccountManager;
                getModel.emailContact = obj.strEmailContact;
                getModel.phoneContact = obj.strPhoneContact;
                getModel.strAddress = obj.strAddress;
                getModel.strCountry = obj.strCountry;
                getModel.postalAddress = obj.strPostalCode;
                getModel.stateID = obj.stateID;
                getModel.companyName = obj.companyName;
                getModel.ABN = obj.ABN;
                getModel.suburb = obj.suburb;
                if (objPicture.id > 0)
                {
                    getModel.pictureID = objPicture.id;
                }
                _FacilitiesSetting.UpdateFacilities(getModel);
            }

            return RedirectToAction("FacilitySetting", new { FacilityID = obj.FacilitiesNo });
        }

        #endregion

        #region Facility Users

        [LogActionFilter]
        public ActionResult AddFacilityUsers(int? ID, int FacilityID)
        {

            FacilityUserModel obj = new FacilityUserModel();
            if (ID != null && ID > 0)
            {
                User getObj = _LoginService.getAccountUserByID(Convert.ToInt32(ID));
                obj = new FacilityUserModel();
                obj.strUserName = getObj.userName;
                obj.emailAddress = getObj.emailAddress;
                obj.isActive = Convert.ToBoolean(getObj.isAcive);
                obj.id = getObj.ID;
                obj.FacilityID = FacilityID;
            }
            else
            {
                obj.strPassowrd = Convert.ToString(Common.RandomString(10));
            }
            return View(obj);
        }


        [LogActionFilter]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddFacilityUsers(FacilityUserModel obj)
        {
            if (!ModelState.IsValid)
                return View(obj);
            if (obj.id > 0)//Update
            {
                User objModel = _LoginService.getAccountUserByID(obj.id);
                objModel.ID = obj.id;
                objModel.emailAddress = obj.emailAddress;
                objModel.isAcive = obj.isActive;//By default Active is TRUE
                _LoginService.UpdateUser(objModel);
            }
            else//Insert New
            {
                if (!_LoginService.IsUserExist(obj.strUserName))
                {
                    obj.memberShipExpiryDate = DateTime.Now.AddYears(1);
                    //Fill model
                    User objModel = new User();
                    objModel.emailAddress = obj.emailAddress;
                    objModel.password = obj.strPassowrd;
                    objModel.userName = obj.strUserName;
                    objModel.memberShipExpiryDate = obj.memberShipExpiryDate;
                    objModel.userTypeID = Convert.ToInt32(Common.UserType.User);
                    objModel.isAcive = obj.isActive;//By default Active is TRUE
                    objModel.isDelete = false;
                    if (_LoginService.saveUsers(objModel))
                    {
                        //Insert Maping
                        User_Mapping objMapping = new User_Mapping();
                        objMapping.UserID = objModel.ID;
                        objMapping.FacilityID = obj.FacilityID;
                        _FacilitiesSetting.SaveAccountMgrFacilityMapping(objMapping);

                        //Send email
                        string getSuperAdminEmailID = _LoginService.getSuperAdminEmailId();
                        string toEmail = objModel.emailAddress;
                        string subject = Common.getResourceValue("%RegisterEmail.Subject%");

                        string getHtml = System.IO.File.ReadAllText(Server.MapPath(@"\EmailTemplate\RegisterEmail.html"));
                        getHtml = getHtml.Replace("%userName%", objModel.userName);
                        getHtml = getHtml.Replace("%Password%", objModel.password);
                        getHtml = getHtml.Replace("%LiveSiteURL%", Common.getResourceValue("%LiveSiteURL%"));

                        Common.sendEmail(toEmail, subject, getHtml, "", getSuperAdminEmailID);

                    }
                    else
                    {
                        obj.msg = "server error!";
                        return View(obj);
                    }
                }
                else
                {
                    obj.msg = "User name already exists";
                    return View(obj);
                }
            }
            return RedirectToAction("FacilitySetting", new { FacilityID = obj.FacilityID });
        }

        [LogActionFilter]
        public ActionResult DeleteFacilityUser(int? ID)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ID)))
            {
                User objModel = _LoginService.getAccountUserByID(Convert.ToInt32(ID));
                objModel.isDelete = true;
                _LoginService.UpdateUser(objModel);
                //Mark delete = true which have user created by him

            }
            return RedirectToAction("AccountOwnerSetting");
        }



        [HttpGet]
        [LogActionFilter]
        public JsonResult getFacilityUsers(int FacilityID)
        {
            List<AccountListModel> objList = new List<AccountListModel>();

            var oldModel = _FacilitiesSetting.getAllFacilityUsers(FacilityID);
            foreach (User item in oldModel)
            {
                AccountListModel obj = new AccountListModel();
                obj.userName = item.userName;
                obj.emailAddress = item.emailAddress;
                obj.isActive = Convert.ToBoolean(item.isAcive);
                obj.memberShipExpiryDate = Convert.ToDateTime(item.memberShipExpiryDate);
                obj.isDeleted = Convert.ToBoolean(item.isDelete);
                obj.id = item.ID;
                objList.Add(obj);
            }
            return Json(objList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult getfacilitylst()
        {
            List<MenuModel> objList = new List<MenuModel>();

            IList<facilityMenu> objFacilityLst = _LoginService.getFacilityByUserID(Convert.ToInt32(Session["userID"]));
            if (objFacilityLst.Count > 0)
            {
                foreach (var item in objFacilityLst)
                {
                    MenuModel obj = new MenuModel();
                    obj.facilityName = item.facilityName;
                    obj.userID = item.userID;
                    obj.userRole = item.userRole;
                    obj.facilityID = item.facilityID;
                    objList.Add(obj);
                }
            }
            return Json(objList, JsonRequestBehavior.AllowGet);
        }

        [LogActionFilter]
        public ActionResult Addfacility(int? id)
        {
            FacilityModel obj = new FacilityModel();
            if (id > 0)
            {
                Facility objget = _LoginService.getFacilityByID(Convert.ToInt32(id));
                obj.FacilityName = objget.facilityName;
                obj.id = objget.id;
            }
            obj.userID = Convert.ToInt32(Session["UserID"]);
            return View(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LogActionFilter]
        public ActionResult Deletefacility(int? id)
        {
            _LoginService.deleteFacilityByID(Convert.ToInt32(id));
            return RedirectToAction("AccountOwnerSetting");
        }


        [HttpPost]
        [LogActionFilter]
        [ValidateAntiForgeryToken]
        public ActionResult Addfacility(FacilityModel obj)
        {
            if (!ModelState.IsValid)
                return View(obj);
            if (obj.id > 0)//Update
            {
                Facility objget = _LoginService.getFacilityByID(Convert.ToInt32(obj.id));
                objget.facilityName = obj.FacilityName;
                _LoginService.UpdateFacility(objget);
            }
            else//Insert
            {
                Facility objFac = new Facility();
                objFac.facilityName = obj.FacilityName;
                objFac.userID = obj.userID;
                _LoginService.saveFacility(objFac);

                //Update in User Table
                User getUser = _LoginService.getAccountUserByID(obj.userID);
                getUser.noOfFacilities = Convert.ToInt32((getUser.noOfFacilities) + 1);
                _LoginService.UpdateUser(getUser);
            }

            return RedirectToAction("AccountOwnerSetting");
        }

        #endregion

    }
}