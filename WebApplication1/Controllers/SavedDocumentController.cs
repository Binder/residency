﻿using Residency.Data;
using Residency.Services;
using Residency.Services.FacilitiesForms;
using Residency.Services.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using WebApplication1.App_Start;
using WebApplication1.Models.SaveDocumets;

namespace WebApplication1.Controllers
{

    [LogActionFilter]
    public class SavedDocumentController : Controller
    {
        #region Feilds

        private readonly IFacilitiesOwnerSettingService _FacilitiesOwnerSetting;
        private readonly ILoginService _LoginService;
        private readonly IDocumentService _DocumentPersoanlInfo;

        #endregion

        #region constructor

        public SavedDocumentController()
        {
            this._LoginService = DependencyResolver.Current.GetService<LoginService>();
            this._DocumentPersoanlInfo = DependencyResolver.Current.GetService<DocumentService>();
            this._FacilitiesOwnerSetting = DependencyResolver.Current.GetService<FacilitiesOwnerSettingService>();
        }


        #endregion

        #region Document Methods

        /// <summary>
        /// Saved Document
        /// </summary>
        /// <returns></returns>
        public ActionResult SavedDocument()
        {
            return View();
        }

        /// <summary>
        /// Get Grid of Saved Documents
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [LogActionFilter]
        public JsonResult GetSavedDocuments()
        {
            List<DocumentModel> objList = new List<DocumentModel>();

            var oldModel = _DocumentPersoanlInfo.GetAllSavedDocuments(Convert.ToInt32(Session["UserID"]));
            foreach (SavedDocument item in oldModel)
            {
                DocumentModel obj = new DocumentModel();
                if (item.DateCreated != null)
                {
                    obj.DateCreated = Convert.ToDateTime(item.DateCreated);
                }
                obj.Id = item.Id;
                obj.Progress = item.Progress;
                obj.UserName = item.UserName;
                obj.FacilityName = item.FacilityName;
                objList.Add(obj);
            }
            return Json(objList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Add NEw Document
        /// </summary>
        /// <returns></returns>
        public int AddNewDocument(string ScreenName)
        {
            Document obj = new Document();
            obj.IsDeleted = false;
            obj.UserID = Convert.ToInt32(Session["UserID"]);
            obj.DateCreated = DateTime.Now;
            obj.Progress = ScreenName;
            _DocumentPersoanlInfo.SaveDocument(obj);
            return obj.Id;
        }

        /// <summary>
        /// Delete Document
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public ActionResult DeleteDocument(int Id)
        {
            Document obj = _DocumentPersoanlInfo.GetDocumentID(Id);
            obj.IsDeleted = true;
            _DocumentPersoanlInfo.UpdateDocument(obj);
            return RedirectToAction("SavedDocument");
        }


        #endregion

        #region Index

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [LogActionFilter]
        [HttpGet]
        public ActionResult Index(int? DocumentID)
        {
            DocumentIndexModel obj = new DocumentIndexModel();
            if (DocumentID > 0)
            {
                obj.ID = Convert.ToInt32(DocumentID);
            }
            return View(obj);
        }

        #endregion

        #region Personal Information

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ResidentAndContactDetails(int? DocumentID)
        {
            //Get doc
            ResidentAndContactDetailsModel obj = new ResidentAndContactDetailsModel();
            if (DocumentID > 0) //In case of Edit
            {
                DocumentPersonalDetail getObj = _DocumentPersoanlInfo.getPersonalDetailByDocumentID(Convert.ToInt32(DocumentID));
                if (getObj != null)
                {
                    obj = convertModelView(getObj);
                }
            }
            BindDropDowns(obj);
            return PartialView("_ResidentAndContactDetails", obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResidentAndContactDetails(ResidentAndContactDetailsModel obj)
        {
            if (obj.DocPerDetid > 0)
            {
                DocumentPersonalDetail getObj = _DocumentPersoanlInfo.getPersonalDetailByID(Convert.ToInt32(obj.DocPerDetid));
                getObj = convertModelSave(obj, getObj);
                _DocumentPersoanlInfo.UpdatePersonalDocument(getObj);
                UpdateAuthorisedRepresentativeItems(obj.strArrLst, obj.DocPerDetid);
                return RedirectToAction("Index", new { DocumentID = getObj.DocumentId });
            }
            else
            {
                DocumentPersonalDetail SaveObj = new DocumentPersonalDetail();
                //Create Document 
                int intDocumentID = AddNewDocument("Screen 1");
                obj.DocumentID = intDocumentID;
                //Save
                _DocumentPersoanlInfo.SavePersonalDocument(convertModelSave(obj, SaveObj));
                //Save Authorised
                SaveAuthorisedRepresentativeItems(obj.strArrLst, SaveObj.Id);

                return RedirectToAction("Index", new { DocumentID = SaveObj.DocumentId });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strString"></param>
        /// <param name="DocumentID"></param>
        [NonAction]
        public void SaveAuthorisedRepresentativeItems(string strString, int DocumentID)
        {
            if (!string.IsNullOrEmpty(strString))
            {
                string[] SptItems = strString.Split('æ');
                foreach (var item in SptItems)
                {
                    string[] SptData = item.Split('₧');
                    AuthorisedRepresentativeItem obj = new AuthorisedRepresentativeItem();
                    obj.DocumentPerDetID = DocumentID;
                    obj.RepresenterTypeId = Convert.ToInt32(SptData[1]);
                    obj.RepresenterTitleId = Convert.ToInt32(SptData[2]);
                    obj.RepresenterFirstName = Convert.ToString(SptData[3]);
                    obj.RepresenterMiddleName = Convert.ToString(SptData[4]);
                    obj.RepresenterLastName = Convert.ToString(SptData[5]);
                    obj.RepresenterAddress = Convert.ToString(SptData[6]);
                    obj.RepresenterSuburb = Convert.ToString(SptData[7]);
                    obj.RepresenterPostalCode = Convert.ToString(SptData[8]);
                    obj.RepresenterEvidenceOfAuthorityPro = Convert.ToBoolean(SptData[9]);
                    _FacilitiesOwnerSetting.SaveAuthorisedRepresentative(obj);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strString"></param>
        /// <param name="DocumentID"></param>
        [NonAction]
        public void UpdateAuthorisedRepresentativeItems(string strString, int documentID)
        {
            _FacilitiesOwnerSetting.DeleteAuthorisedRepresentative(documentID);
            SaveAuthorisedRepresentativeItems(strString, documentID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [NonAction]
        public ResidentAndContactDetailsModel BindDropDowns(ResidentAndContactDetailsModel obj)
        {
            //Bind DropDowns
            foreach (var c in _FacilitiesOwnerSetting.GetAllStates())
                obj.AdressStates.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.State) });

            foreach (var c in _FacilitiesOwnerSetting.GetAllStates())
                obj.BillingStates.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.BillingState) });

            foreach (var c in _FacilitiesOwnerSetting.GetAllStates())
                obj.EmergancyStates.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.EmergencyContactState) });

            //foreach (var c in _FacilitiesOwnerSetting.GetAllStates())
            //    obj.Athority1States.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.AutherityState) });

            foreach (var c in _FacilitiesOwnerSetting.GetTitleOptions())
                obj.TitleOptions.Add(new SelectListItem { Text = c.Name, Value = c.id.ToString(), Selected = (c.id == obj.TitleId) });

            foreach (var c in _FacilitiesOwnerSetting.GetMaritalStatus())
                obj.MaritalStatus.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.MaritalStatusID) });

            foreach (var c in _FacilitiesOwnerSetting.GetTitleOptions())
                obj.EmergencyContactTitle.Add(new SelectListItem { Text = c.Name, Value = c.id.ToString(), Selected = (c.id == obj.EmergencyContactTitleID) });

            foreach (var c in _FacilitiesOwnerSetting.GetRelationships())
                obj.EmergencyRelationship.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.EmergencyRelationShipID) });

            //Authorised Representative Items
            clsAuthRepItems objItem = new clsAuthRepItems();

            foreach (var c in _FacilitiesOwnerSetting.GetRepresenterTypes())
                objItem.RepresenterTypes.Add(new SelectListItem { Text = c.TypeName, Value = c.ID.ToString(), Selected = (c.ID == obj.EmergencyRelationShipID) });

            foreach (var c in _FacilitiesOwnerSetting.GetTitleOptions())
                objItem.RepresenterTitle.Add(new SelectListItem { Text = c.Name, Value = c.id.ToString(), Selected = (c.id == obj.EmergencyRelationShipID) });

            foreach (var c in _FacilitiesOwnerSetting.GetAllStates())
                objItem.RepresenterStates.Add(new SelectListItem { Text = c.Name, Value = c.ID.ToString(), Selected = (c.ID == obj.EmergencyRelationShipID) });

            obj.clsAuthRepItemsItems = objItem;

            return obj;
        }

        /// <summary>
        /// convert Mode lSave
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [NonAction]
        public DocumentPersonalDetail convertModelSave(ResidentAndContactDetailsModel obj, DocumentPersonalDetail SaveObj)
        {

            SaveObj.DocumentId = obj.DocumentID;

            //Personal Information
            SaveObj.TitleId = obj.TitleId;
            SaveObj.FirstName = obj.FirstName;
            SaveObj.MiddleName = obj.MiddleName;
            SaveObj.LastName = obj.LastName;
            SaveObj.DOB = (string.IsNullOrEmpty(obj.DOB) == true ? Convert.ToDateTime("1/1/1991") : Convert.ToDateTime(obj.DOB));
            SaveObj.MaritalStatusID = obj.MaritalStatusID;

            //Emergancy
            SaveObj.EmergencyAddress = obj.EmergencyContactStreetAddress;
            SaveObj.EmergencySuburb = obj.EmergencyContactSuburb;
            SaveObj.EmergencyPostalCode = obj.EmergencyContactPostalCode;
            SaveObj.EmergencyHomeNumber = obj.EmergencyContactHomePhone;
            SaveObj.EmergencyMobileNumber = obj.EmergencyContactMobileNumber;
            SaveObj.EmergencyPostalCode = obj.EmergencyContactPostalCode;
            SaveObj.EmergencyTitleID = obj.EmergencyContactTitleID;
            SaveObj.EmergencyFirstName = obj.EmergencyContactFirstName;
            SaveObj.EmergencyMiddleName = obj.EmergencyContactMiddleName;
            SaveObj.EmergencyLastName = obj.EmergencyContactLastName;
            SaveObj.EmergencyDOB = (string.IsNullOrEmpty(obj.EmergencyContactDOB) == true ? Convert.ToDateTime("1/1/1991") : Convert.ToDateTime(obj.EmergencyContactDOB));
            SaveObj.EmergencyStateID = obj.EmergencyContactState;
            SaveObj.EmergencyRelationID = obj.EmergencyRelationShipID;
            SaveObj.EmergencyAuthRep = obj.EmergencyAuthorisedRepresentative;

            //Address
            SaveObj.Suburb = obj.Suburb;
            SaveObj.StateID = obj.State;
            SaveObj.PostCode = obj.PostalCode;
            SaveObj.Address = obj.StreetAddress;

            //Billing Address
            SaveObj.BillingCName = obj.BillingCName;
            SaveObj.BillingAddress = obj.BillingStreetAddress;
            SaveObj.BillingPostalCode = obj.BillingPostalCode;
            SaveObj.BillingSuburb = obj.BillingSuburb;
            SaveObj.BillingStateID = obj.BillingState;
            SaveObj.BillingPrivateAndCon = obj.PrivateAndConfidential;
            return SaveObj;
        }

        /// <summary>
        /// convert Model View
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [NonAction]
        public ResidentAndContactDetailsModel convertModelView(DocumentPersonalDetail obj)
        {
            ResidentAndContactDetailsModel viewObj = new ResidentAndContactDetailsModel();

            viewObj.DocumentID = Convert.ToInt32(obj.DocumentId);
            viewObj.DocPerDetid = obj.Id;
            //Personal Information
            viewObj.TitleId = Convert.ToInt32(obj.TitleId);
            viewObj.FirstName = obj.FirstName;
            viewObj.LastName = obj.LastName;
            viewObj.DOB = obj.DOB == null ? "" : Convert.ToDateTime(obj.DOB).ToString(Common.getDateFormat());
            viewObj.MaritalStatusID = Convert.ToInt32(obj.MaritalStatusID);
            viewObj.MiddleName = obj.MiddleName;

            //Emergancy
            viewObj.EmergencyAuthorisedRepresentative = Convert.ToBoolean(obj.EmergencyAuthRep);
            viewObj.EmergencyRelationShipID = Convert.ToInt32(obj.EmergencyRelationID);
            viewObj.EmergencyContactStreetAddress = obj.EmergencyAddress;
            viewObj.EmergencyContactSuburb = obj.EmergencySuburb;
            viewObj.EmergencyContactPostalCode = obj.EmergencyPostalCode;
            viewObj.EmergencyContactHomePhone = obj.EmergencyHomeNumber;
            viewObj.EmergencyContactMobileNumber = obj.EmergencyMobileNumber;
            viewObj.EmergencyContactTitleID = Convert.ToInt32(obj.EmergencyTitleID);
            viewObj.EmergencyContactFirstName = obj.EmergencyFirstName;
            viewObj.EmergencyContactMiddleName = obj.EmergencyMiddleName;
            viewObj.EmergencyContactLastName = obj.EmergencyLastName;
            viewObj.EmergencyContactDOB = obj.EmergencyDOB == null ? "" : Convert.ToDateTime(obj.EmergencyDOB).ToString(Common.getDateFormat());
            viewObj.EmergencyContactState = Convert.ToInt32(obj.EmergencyStateID);
            viewObj.EmergencyRelationShipID = Convert.ToInt32(obj.EmergencyRelationID);

            //Address
            viewObj.Suburb = obj.Suburb;
            viewObj.State = Convert.ToInt32(obj.StateID);
            viewObj.PostalCode = obj.PostCode;
            viewObj.StreetAddress = obj.Address;

            //Billing Address
            viewObj.BillingCName = obj.BillingCName;
            viewObj.BillingStreetAddress = obj.BillingAddress;
            viewObj.BillingPostalCode = obj.BillingPostalCode;
            viewObj.BillingSuburb = obj.BillingSuburb;
            viewObj.BillingState = Convert.ToInt32(obj.BillingStateID);
            viewObj.PrivateAndConfidential = Convert.ToBoolean(obj.BillingPrivateAndCon);

            string items = "";
            int Index = 0;
            foreach (var item in obj.AuthorisedRepresentativeItems)
            {
                if (!string.IsNullOrEmpty(items))
                {
                    items += "æ";
                }
                items += Convert.ToString(Index) + "₧" + Convert.ToString(item.RepresenterTypeId) + "₧" + Convert.ToString(item.RepresenterTitleId) + "₧" +
                    Convert.ToString(item.RepresenterFirstName) + "₧" + Convert.ToString(item.RepresenterMiddleName) + "₧" +
                    Convert.ToString(item.RepresenterLastName) + "₧" + Convert.ToString(item.RepresenterAddress) + "₧" +
                    Convert.ToString(item.RepresenterSuburb) + "₧" + Convert.ToString(item.RepresenterPostalCode) + "₧" +
                    Convert.ToBoolean(item.RepresenterEvidenceOfAuthorityPro);
                Index = (Index + 1);
            }
            viewObj.strArrLst = items;

            return viewObj;
        }



        #endregion

        #region Occupancy Details

        /// <summary>
        /// _OccupancyDetails
        /// </summary>
        /// <returns></returns>
        public ActionResult _OccupancyDetails(int documentID)
        {
            OccupancyDetailsModel obj = new OccupancyDetailsModel();
            DocumentOccupancyDetail getObj =
                _DocumentPersoanlInfo.GetOccupancyDetailByDocumentId(documentID);
            if (getObj != null)
            {
                obj.ID = getObj.Id;
                obj.Wing = getObj.Wing;
                if (!string.IsNullOrEmpty(Convert.ToString(getObj.AgreedEntryDate)))
                {
                    obj.AgreedEntryDate = Convert.ToDateTime(getObj.AgreedEntryDate).ToString(Common.getDateFormat());
                }
                if (!string.IsNullOrEmpty(Convert.ToString(getObj.PeriodOfPreEntryFrom)))
                {
                    obj.PeriodOfPreEntryFrom = Convert.ToDateTime(getObj.PeriodOfPreEntryFrom).ToString(Common.getDateFormat()); ;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(getObj.PeriodOfPreEntryTo)))
                {
                    obj.PeriodOfPreEntryTo = Convert.ToDateTime(getObj.PeriodOfPreEntryTo).ToString(Common.getDateFormat()); ;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(getObj.DateCareAndServices)))
                {
                    obj.DateCareServiceCommmenced = Convert.ToDateTime(getObj.DateCareAndServices).ToString(Common.getDateFormat());
                }
                if (!string.IsNullOrEmpty(Convert.ToString(getObj.DateCareAndServicesCeased)))
                {
                    obj.DateCareServiceCeased = Convert.ToDateTime(getObj.DateCareAndServicesCeased).ToString(Common.getDateFormat());
                }
                obj.Transferred = Convert.ToBoolean(getObj.Transferred);
                obj.PreEntryLeave = Convert.ToBoolean(getObj.PreEntryLeave);
            }




            var getDet = _DocumentPersoanlInfo.FacilityDetailByUserID(Convert.ToInt32(Session["UserID"]));
            obj.FacilityName = getDet.ownerAccountSett_Name;
            obj.FacilityAdress = getDet.ownerAccountSett_Address;
            obj.FacilityPostalCode = getDet.ownerAccountSett_PostalCode;
            obj.FacilityState = (getDet.ownerAccountSett_State == "--Select--" ? "" : getDet.ownerAccountSett_State);
            obj.FacilitySuburb = getDet.ownerAccountSett_Suburb;

            obj.ProviderName = getDet.AccountSettingSett_Name;
            obj.ProviderSuburb = getDet.AccountSetting_Suburb;
            obj.ProviderAdress = getDet.AccountSetting_Address;
            obj.ProviderState = (getDet.AccountSetting_State == "--Select--" ? "" : getDet.AccountSetting_State);
            obj.ProviderPostalCode = getDet.AccountSetting_PostalCode;
            obj.ProviderABN = getDet.AccountSetting_ABN;
            obj.DocumentId = documentID;

            return View(obj);
        }

        /// <summary>
        /// _Occupancy Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _OccupancyDetails(OccupancyDetailsModel obj)
        {
            DocumentOccupancyDetail saveObj = new DocumentOccupancyDetail();
            if (obj.ID > 0)
            {
                saveObj = _DocumentPersoanlInfo.GetOccupancyDetailByDocumentId(obj.DocumentId);
            }
            saveObj.Wing = obj.Wing;
            if (!string.IsNullOrEmpty(obj.AgreedEntryDate))
            {
                saveObj.AgreedEntryDate = DateTime.ParseExact(obj.AgreedEntryDate, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrEmpty(obj.PeriodOfPreEntryFrom))
            {
                saveObj.PeriodOfPreEntryFrom = DateTime.ParseExact(obj.PeriodOfPreEntryFrom, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrEmpty(obj.PeriodOfPreEntryTo))
            {
                saveObj.PeriodOfPreEntryTo = DateTime.ParseExact(obj.PeriodOfPreEntryTo, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrEmpty(obj.DateCareServiceCommmenced))
            {
                saveObj.DateCareAndServices = DateTime.ParseExact(obj.DateCareServiceCommmenced, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(obj.DateCareServiceCeased))
            {
                saveObj.DateCareAndServicesCeased = DateTime.ParseExact(obj.DateCareServiceCeased, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            }

            saveObj.PreEntryLeave = obj.PreEntryLeave;
            saveObj.Transferred = obj.Transferred;
            if (obj.ID > 0)
            {
                _DocumentPersoanlInfo.UpdateOccupancyDetails(saveObj);
            }
            else
            {
                if (obj.DocumentId == 0)
                {
                    //Create Document 
                    int intDocumentID = AddNewDocument("Screen 2");
                    obj.DocumentId = intDocumentID;
                }
                else
                {
                    Document objDocument = _DocumentPersoanlInfo.GetDocumentID(Convert.ToInt32(obj.DocumentId));
                    objDocument.Progress = "Screen 2";
                    _DocumentPersoanlInfo.UpdateDocument(objDocument);
                }
                //Save
                saveObj.DocumentID = obj.DocumentId;
                _DocumentPersoanlInfo.SaveOccupancyDetails(saveObj);
            }
            return RedirectToAction("Index", new { DocumentID = saveObj.DocumentID });
        }

        #endregion

        #region IncomeOrAssests

        /// <summary>
        /// Income Or Assests
        /// </summary>
        /// <param name="documentID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult _IncomeOrAssests(int documentID)
        {
            IncomeAndAssetsModel obj = new IncomeAndAssetsModel();
            if (documentID > 0)
            {
                IncomeAndAssest getObj = new IncomeAndAssest();
                getObj = _DocumentPersoanlInfo.getIncomeAndAssestByDocumentId(documentID);
                //Convert model
                if (getObj != null)
                {
                    obj.ID = Convert.ToInt32(getObj.ID);
                    obj.DocumentID = Convert.ToInt32(getObj.DocumentID);
                    //Asset Income Section
                    obj.GovtIncomeSuppPayment = Convert.ToDecimal(getObj.GovtIncomeSuppPayment);
                    obj.OtherIncome = Convert.ToDecimal(getObj.OtherIncome);
                    obj.YourIncome = Convert.ToDecimal(getObj.YourIncome);
                    obj.AccommodationBond = Convert.ToDecimal(getObj.AccommodationBond);
                    obj.NetRetirementVillageEntCont = Convert.ToDecimal(getObj.NetRetirementVillageEntCont);
                    obj.FinancialAccounts = Convert.ToDecimal(getObj.FinancialAccounts);
                    obj.Shares = Convert.ToDecimal(getObj.Shares);
                    obj.ManagedInvestment = Convert.ToDecimal(getObj.ManagedInvestment);
                    obj.AssessableIncomeStreams = Convert.ToDecimal(getObj.AssessableIncomeStreams);
                    obj.ForeginAssets = Convert.ToDecimal(getObj.ForeginAssets);
                    obj.RealEstateAndBusinessInt = Convert.ToDecimal(getObj.RealEstateAndBusinessInt);
                    obj.PrivateTrusts = Convert.ToDecimal(getObj.PrivateTrusts);
                    obj.GiftsOrDeprivation = Convert.ToDecimal(getObj.GiftsOrDeprivation);
                    obj.OtherAssets = Convert.ToDecimal(getObj.OtherAssets);
                    //--Net Assets ---
                    obj.NetAssets = Convert.ToDouble(getObj.GovtIncomeSuppPayment + getObj.OtherIncome + getObj.YourIncome + getObj.AccommodationBond
                        + getObj.NetRetirementVillageEntCont + getObj.FinancialAccounts + getObj.Shares + getObj.ManagedInvestment + getObj.AssessableIncomeStreams + getObj.ForeginAssets
                        + getObj.RealEstateAndBusinessInt + getObj.PrivateTrusts + getObj.GiftsOrDeprivation + getObj.OtherAssets);
                    //--Net Debits--
                    obj.NetDebts = Convert.ToDouble(getObj.DebtsPersonalLoans + getObj.DebtsOther);
                    //--Net Income--
                    obj.NetIncome = obj.NetAssets - obj.NetDebts;
                    //End:- AssetIncome Section
                    obj.FullPension = Convert.ToBoolean(getObj.FullPension);
                    obj.PartPension = Convert.ToBoolean(getObj.PartPension);
                    //Debit Section
                    obj.DebtsPersonalLoans = Convert.ToDecimal(getObj.DebtsPersonalLoans);
                    obj.DebtsOther = Convert.ToDecimal(getObj.DebtsOther);
                    //End:-Debit Section
                    obj.AsstRedAmtPrisonerOfWar = Convert.ToDecimal(getObj.AsstRedAmtPrisonerOfWar);
                    obj.RerspiteCareOnly = Convert.ToBoolean(getObj.RerspiteCareOnly);
                    obj.DeclineToDeclare = Convert.ToBoolean(getObj.DeclineToDeclare);
                    obj.SelfFundedRetiree = Convert.ToBoolean(getObj.SelfFundedRetiree);
                    obj.PensionNumber = Convert.ToInt32(getObj.PensionNumber);


                }
                else
                {
                    obj.DocumentID = documentID;
                    obj.DeclineToDeclare = true;
                }
            }
            else
            {
                obj.DocumentID = documentID;
                obj.DeclineToDeclare = true;
            }
            return View(obj);
        }

        /// <summary>
        /// Income Or Assests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _IncomeOrAssests(IncomeAndAssetsModel obj)
        {
            IncomeAndAssest objSave = new IncomeAndAssest();
            if (obj.ID > 0)
            {
                objSave = _DocumentPersoanlInfo.getIncomeAndAssestByDocumentId(obj.DocumentID);
            }
            objSave.DocumentID = obj.DocumentID;
            objSave.GovtIncomeSuppPayment = obj.GovtIncomeSuppPayment;
            objSave.OtherIncome = obj.OtherIncome;
            objSave.YourIncome = obj.YourIncome;
            objSave.AccommodationBond = obj.AccommodationBond;
            objSave.NetRetirementVillageEntCont = obj.NetRetirementVillageEntCont;
            objSave.FinancialAccounts = obj.FinancialAccounts;
            objSave.Shares = obj.Shares;
            objSave.ManagedInvestment = obj.ManagedInvestment;
            objSave.AssessableIncomeStreams = obj.AssessableIncomeStreams;
            objSave.ForeginAssets = obj.ForeginAssets;
            objSave.RealEstateAndBusinessInt = obj.RealEstateAndBusinessInt;
            objSave.PrivateTrusts = obj.PrivateTrusts;
            objSave.GiftsOrDeprivation = obj.GiftsOrDeprivation;
            objSave.OtherAssets = obj.OtherAssets;
            objSave.FullPension = obj.FullPension;
            objSave.PartPension = obj.PartPension;
            objSave.DebtsPersonalLoans = obj.DebtsPersonalLoans;
            objSave.DebtsOther = obj.DebtsOther;
            objSave.AsstRedAmtPrisonerOfWar = obj.AsstRedAmtPrisonerOfWar;
            objSave.RerspiteCareOnly = obj.RerspiteCareOnly;
            objSave.DeclineToDeclare = Convert.ToBoolean(obj.DeclineToDeclare);
            objSave.SelfFundedRetiree = Convert.ToBoolean(obj.SelfFundedRetiree);
            objSave.PensionNumber = obj.PensionNumber;


            //Check if  Document not created
            if (obj.DocumentID == 0)
            {
                //Create Document 
                int intDocumentID = AddNewDocument("Screen 3");
                objSave.DocumentID = intDocumentID;
            }
            else
            {
                Document objDocument = _DocumentPersoanlInfo.GetDocumentID(Convert.ToInt32(obj.DocumentID));
                objDocument.Progress = "Screen 3";
                _DocumentPersoanlInfo.UpdateDocument(objDocument);
            }
            if (obj.ID > 0)//Update
            {
                _DocumentPersoanlInfo.UpdateIncomeOrAssests(objSave);
            }
            else
            {
                //Save
                _DocumentPersoanlInfo.SaveIncomeOrAssests(objSave);
            }
            return RedirectToAction("Index", new { DocumentID = objSave.DocumentID });

        }


        #endregion

        #region AccommodationPayment

        [HttpGet]
        public ActionResult _AccommodationPayment(int documentId)
        {
            AccommodationPaymentModel obj = new AccommodationPaymentModel();

            if (documentId > 0)
            {
                AccommodationPayment objAP = _DocumentPersoanlInfo.GetAccommodationPayment(documentId);

                if (objAP != null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<AccommodationPayment, AccommodationPaymentModel>();
                    });
                    IMapper mapper = config.CreateMapper();
                    obj = mapper.Map<AccommodationPayment, AccommodationPaymentModel>(objAP);

                    obj.ActualPaymentDate1 = objAP.ActualPaymentDate.Value.Date.ToString("dd-mm-yyyy");
                    obj.AdmissionsDate1 = objAP.AdmissionsDate.Value.Date.ToString("dd-mm-yyyy");
                    obj.ProjectPaymentDecDate1 = objAP.ProjectPaymentDecDate.Value.Date.ToString("dd-mm-yyyy");
                }
            }

            obj.DocumentID = documentId;

            foreach (var pm in _DocumentPersoanlInfo.GetPaymentMethod())
                obj.PaymentMethods.Add(new SelectListItem { Text = pm.PaymentMethod1, Value = pm.ID.ToString(), Selected = (pm.ID == obj.PaymentMethod) });

            foreach (var dpm in _DocumentPersoanlInfo.GetDrawdownRecoveryMethod())
                obj.DrawdownRecoveryMethods.Add(new SelectListItem { Text = dpm.DrawdownRecMethod, Value = dpm.ID.ToString(), Selected = (dpm.ID == obj.DrawdownRecoMethod) });


            return View(obj);
        }

        [HttpPost]
        public ActionResult _AccommodationPayment(AccommodationPaymentModel obj)
        {

            if (obj.DocumentID == 0)
            {
                //Create Document 
                int intDocumentID = AddNewDocument("Screen 4");
                obj.DocumentID = intDocumentID;
            }
            else
            {
                Document objDocument = _DocumentPersoanlInfo.GetDocumentID(Convert.ToInt32(obj.DocumentID));
                objDocument.Progress = "Screen 4";
                _DocumentPersoanlInfo.UpdateDocument(objDocument);
            }

            obj.ActualPaymentDate = DateTime.ParseExact(obj.ActualPaymentDate1, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            obj.AdmissionsDate = DateTime.ParseExact(obj.AdmissionsDate1, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            obj.ProjectPaymentDecDate = DateTime.ParseExact(obj.ProjectPaymentDecDate1, "dd-mm-yyyy", CultureInfo.InvariantCulture);

            if (!obj.AccomPayment)
            {
                obj.AccomContribution = true;
            }
            AccommodationPayment objAP = new AccommodationPayment();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AccommodationPaymentModel, AccommodationPayment>();
            });
            IMapper mapper = config.CreateMapper();
            objAP = mapper.Map<AccommodationPaymentModel, AccommodationPayment>(obj);

            obj.ID = _DocumentPersoanlInfo.SaveAccommodationPayment(objAP);

            return RedirectToAction("Index", new { DocumentID = obj.DocumentID });
        }

        #endregion

    }
}