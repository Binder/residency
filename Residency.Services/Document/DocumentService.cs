﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Residency.Data;
using Residency.Services.Model;

namespace Residency.Services.FacilitiesForms
{
    public class DocumentService : IDocumentService
    {
        #region Feilds
        ResidencyAgreementEntities db = new ResidencyAgreementEntities();
        #endregion

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SavePersonalDocument(DocumentPersonalDetail obj)
        {
            try
            {
                db.DocumentPersonalDetails.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        public DocumentPersonalDetail getPersonalDetailByID(int ID)
        {
            var getObj = (from p in db.DocumentPersonalDetails where p.Id == ID select p).ToList();
            if (getObj.Count() > 0)
            {
                return getObj[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        public DocumentPersonalDetail getPersonalDetailByDocumentID(int DocumentID)
        {
            var getObj = (from p in db.DocumentPersonalDetails where p.DocumentId == DocumentID select p).ToList();
            if (getObj.Count() > 0)
            {
                return getObj[0];
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool UpdatePersonalDocument(DocumentPersonalDetail obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        //Documents

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public IList<SavedDocument> GetAllSavedDocuments(int? UserID)
        {
            if (UserID != null)
            {
                return (from p in db.Documents
                        join u in db.Users on p.UserID equals u.ID
                        join FM in db.User_Mapping on u.ID equals FM.UserID
                        join Fac in db.Facilities on FM.FacilityID equals Fac.id
                        where p.UserID == (int)UserID && p.IsDeleted == false
                        select new SavedDocument
                        {
                            Id = p.Id,
                            Progress = p.Progress,
                            DateCreated = (DateTime)p.DateCreated,
                            UserID = (int)p.UserID,
                            UserName = u.userName,
                            FacilityName = Fac.facilityName
                        }).ToList();
            }
            else
            {
                return (from p in db.Documents
                        where p.IsDeleted == false
                        join u in db.Users on p.UserID equals u.ID
                        join FM in db.User_Mapping on u.ID equals FM.UserID
                        join Fac in db.Facilities on FM.FacilityID equals Fac.id
                        where p.IsDeleted == false
                        select new SavedDocument
                        {
                            Id = p.Id,
                            Progress = p.Progress,
                            DateCreated = (DateTime)p.DateCreated,
                            UserID = (int)p.UserID,
                            UserName = u.userName,
                            FacilityName = Fac.facilityName
                        }).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SaveDocument(Document obj)
        {
            try
            {
                db.Documents.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateDocument(Document obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Get Document ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Document GetDocumentID(int id)
        {
            var q = (from p in db.Documents where p.Id == id select p).ToList();
            if (q.Count() > 0)
            {
                return q[0];
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Facility Detail By UserID
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public GetFacilityDetailByUserID FacilityDetailByUserID(int userID)
        {
            var q = from p in db.User_Mapping
                    join Fac in db.Facility_Settings on p.FacilityID equals Fac.facilityID
                    join FS in db.Facilities on Fac.facilityID equals FS.id
                    join AS in db.Facility_Owner_AccountSetting on FS.userID equals AS.loginUserID
                    select new GetFacilityDetailByUserID
                    {
                        ownerAccountSett_Name = AS.User.userName,
                        ownerAccountSett_Address = AS.strAddress,
                        ownerAccountSett_PostalCode = AS.strPostCode,
                        ownerAccountSett_State = AS.State.Name,
                        ownerAccountSett_Suburb = AS.suburb,

                        AccountSettingSett_Name = Fac.companyName,
                        AccountSetting_Address = Fac.strAddress,
                        AccountSetting_PostalCode = Fac.postalAddress,
                        AccountSetting_State = Fac.State.Name,
                        AccountSetting_ABN = Fac.ABN,
                        AccountSetting_Suburb = Fac.suburb

                    };

            return q.FirstOrDefault();
        }
        /// <summary>
        /// Save Occupancy Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SaveOccupancyDetails(DocumentOccupancyDetail obj)
        {
            try
            {
                db.DocumentOccupancyDetails.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        public DocumentOccupancyDetail GetOccupancyDetailByDocumentId(int DocumentId)
        {
            var q = from p in db.DocumentOccupancyDetails where p.DocumentID == DocumentId select p;
            if (q.Count() > 0)
            {
                return q.SingleOrDefault();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update Occupancy Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateOccupancyDetails(DocumentOccupancyDetail obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Save Income Or Assests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SaveIncomeOrAssests(IncomeAndAssest obj)
        {
            try
            {
                db.IncomeAndAssests.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Update Income Or Assests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateIncomeOrAssests(IncomeAndAssest obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// get Income and Assest By DocumentId
        /// </summary>
        /// <param name="DocumenID"></param>
        /// <returns></returns>
        public IncomeAndAssest getIncomeAndAssestByDocumentId(int DocumentID)
        {
            var q = from p in db.IncomeAndAssests where p.DocumentID == DocumentID select p;
            if (q.Count() > 0)
            {
                return q.SingleOrDefault();
            }
            else
            {
                return null;
            }
        }

        public List<PaymentMethod> GetPaymentMethod()
        {
            var pm = from p in db.PaymentMethods select p;
            return pm.ToList();
        }

        public List<DrawdownRecoveryMethod> GetDrawdownRecoveryMethod()
        {
            var dpm = from o in db.DrawdownRecoveryMethods select o;
            return dpm.ToList();
        }

        public int SaveAccommodationPayment(AccommodationPayment obj)
        {

            db.Entry(obj).State = obj.ID > 0
                ? System.Data.Entity.EntityState.Modified
                : System.Data.Entity.EntityState.Added;

            db.SaveChanges();

            return obj.ID;

        }

        public AccommodationPayment GetAccommodationPayment(int DocumentId)
        {
            return db.AccommodationPayments.FirstOrDefault(ap => ap.DocumentID == DocumentId);
        }
    }
}
