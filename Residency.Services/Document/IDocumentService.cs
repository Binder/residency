﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Residency.Data;
using Residency.Services.Model;

namespace Residency.Services.FacilitiesForms
{
    public interface IDocumentService
    {
        /// <summary>
        /// Get Owner Setting
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        bool SavePersonalDocument(DocumentPersonalDetail obj);

        /// <summary>
        /// get Document By ID
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        DocumentPersonalDetail getPersonalDetailByID(int ID);

        /// <summary>
        /// Update Personal Document
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool UpdatePersonalDocument(DocumentPersonalDetail obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        IList<SavedDocument> GetAllSavedDocuments(int? UserID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool SaveDocument(Document obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <returns></returns>
        DocumentPersonalDetail getPersonalDetailByDocumentID(int DocumentID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdateDocument(Document obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        Document GetDocumentID(int id);

        /// <summary>
        /// Facility Detail By User ID
        /// </summary>
        /// <returns></returns>
        GetFacilityDetailByUserID FacilityDetailByUserID(int userID);

        /// <summary>
        /// Save OccupancyDetails
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        bool SaveOccupancyDetails(DocumentOccupancyDetail obj);

        /// <summary>
        /// Get Occupancy Detail By Id
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        DocumentOccupancyDetail GetOccupancyDetailByDocumentId(int DocumentId);

        /// <summary>
        /// Update Occupancy Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdateOccupancyDetails(DocumentOccupancyDetail obj);

        /// <summary>
        /// Save Income Or Assests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool SaveIncomeOrAssests(IncomeAndAssest obj);

        /// <summary>
        /// Update Income Or Assests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdateIncomeOrAssests(IncomeAndAssest obj);

        /// <summary>
        /// get Income And Assest By DocumentId
        /// </summary>
        /// <param name="DocumenID"></param>
        /// <returns></returns>
        IncomeAndAssest getIncomeAndAssestByDocumentId(int DocumentID);
        
        /// <summary>
        /// Get Payments method
        /// </summary>
        /// <returns></returns>
        List<PaymentMethod> GetPaymentMethod();

        /// <summary>
        /// Get Drawdown Recovery Method
        /// </summary>
        /// <returns></returns>
        List<DrawdownRecoveryMethod> GetDrawdownRecoveryMethod();

        /// <summary>
        /// Insert Or Update AccommodationPayment
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        int SaveAccommodationPayment(AccommodationPayment obj);


        /// <summary>
        /// Get Accommodation Payment
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        AccommodationPayment GetAccommodationPayment(int DocumentId);



    }
}
