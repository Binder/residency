﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Residency.Data;
namespace Residency.Services.FacilitiesForms
{
    public interface IFacilitiesSetting
    {
        /// <summary>
        /// Facility Setting
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        Facility_Settings GetFacilitySettingByUserID(int userID, int FacilityId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdateFacilities(Facility_Settings obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool saveFacility(Facility_Settings obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OwnerID"></param>
        /// <param name="CreatedUserID"></param>
        /// <returns></returns>
        bool SaveAccountMgrFacilityMapping(User_Mapping obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userTypeID"></param>
        /// <returns></returns>
        IList<User> getAllFacilityUsers(int facilityID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        string getFacilityNameByID(int id);

    }
}
