﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Residency.Data;
namespace Residency.Services.FacilitiesForms
{
    public interface  IFacilitiesOwnerSetting
    {
        /// <summary>
        /// Get Owner Setting
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        FacilitiesOwnerDetail GetOwnerSetting(int userID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdateFacilitiesOwnerDetail(Facility_Owner_AccountSetting obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool saveFacility_Owner_Account(Facility_Owner_AccountSetting obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        Facility_Owner_AccountSetting getOwnerSettingForUpdate(int userID);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<State> GetAllStates();

    }
}
