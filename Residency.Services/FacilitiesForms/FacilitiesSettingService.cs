﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Residency.Data;
namespace Residency.Services.FacilitiesForms
{
    public class FacilitiesSettingService : IFacilitiesSettingService
    {
        #region Feilds
        ResidencyAgreementEntities db = new ResidencyAgreementEntities();
        #endregion

        /// <summary>
        /// Facility Setting
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="FacilityNumber"></param>
        /// <returns></returns>
        public Facility_Settings GetFacilitySettingByUserID(int userID, int FacilityId)
        {
            var q = (from p in db.Facility_Settings
                     join f in db.Facilities on p.facilityID equals f.id
                     where p.facilityID == FacilityId && f.userID == userID
                     select p).ToList();
            if (q.Count > 0)
            {
                return q.FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public Facility_Settings GetFacilitySettingByUser(int userID)
        {
            var q = (from p in db.Facility_Settings
                     join up in db.User_Mapping on p.facilityID equals up.FacilityID
                     where up.UserID == userID
                     select p).ToList();
            if (q.Count > 0)
            {
                return q.FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// save Facility
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool saveFacility(Facility_Settings obj)
        {
            try
            {
                db.Facility_Settings.Add(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Update Facilities
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateFacilities(Facility_Settings obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SaveAccountMgrFacilityMapping(User_Mapping obj)
        {
            try
            {
                db.User_Mapping.Add(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// get All Facility Users
        /// </summary>
        /// <param name="OwnerID"></param>
        /// <returns></returns>
        public IList<User> getAllFacilityUsers(int facilityID)
        {
            return (from p in db.Users
                    join m in db.User_Mapping on p.ID equals m.UserID
                    where p.isDelete == false && p.userTypeID == (Int32)Common.UserType.User && m.FacilityID == facilityID
                    select p).ToList();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string getFacilityNameByID(int id)
        {
            string result = string.Empty;
            var q = from f in db.Facilities where f.id == id select f;
            if (q.Count() > 0)
            {
                result = q.ToList()[0].facilityName;

            }
            return result;
        }
    }
}
