﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Residency.Data;

namespace Residency.Services.FacilitiesForms
{
    public class FacilitiesOwnerSetting : IFacilitiesOwnerSetting
    {
        #region Feilds
        ResidencyAgreementEntities db = new ResidencyAgreementEntities();
        #endregion


        /// <summary>
        /// Save
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool saveFacilitiesOwnerDetail(Facility_Owner_AccountSetting obj)
        {
            try
            {
                db.Facility_Owner_AccountSetting.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool UpdateFacilitiesOwnerDetail(Facility_Owner_AccountSetting obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool saveFacility_Owner_Account(Facility_Owner_AccountSetting obj)
        {
            try
            {
                db.Facility_Owner_AccountSetting.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public Facility_Owner_AccountSetting getOwnerSettingForUpdate(int userID)
        {
            var q = (from p in db.Facility_Owner_AccountSetting
                     where p.loginUserID == userID
                     select p).ToList();
            if (q.Count() > 0)
            {
                return q.FirstOrDefault();
            }

            return null;

        }
        /// <summary>
        /// Get Detail
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public FacilitiesOwnerDetail GetOwnerSetting(int userID)
        {
            var q = from u in db.Users
                    join co in db.Facility_Owner_AccountSetting on u.ID equals co.loginUserID into com
                    from p in com.DefaultIfEmpty()
                    where u.ID == userID && u.isDelete == false
                    select new FacilitiesOwnerDetail
                    {
                        accountManager = p.accountManager,
                        membershipExpiry = (DateTime)u.memberShipExpiryDate,
                        strAddress = p.strAddress,
                        strCountry = p.strCountry,
                        strEmailContact = p.emailContact,
                        strPassowrd = u.password,
                        strPhoneContact = p.phoneContact,
                        strPostalCode = p.strPostCode,
                        StateID = (int)(p == null ? 0 : p.StateID),
                        userName = u.userName,
                        FacilitiesID = (p == null ? 0 : p.id)
                    };

            if (q.Count() > 0)
            {
                return q.ToList()[0];
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<State> GetAllStates()
        {
            return (from p in db.States select p).ToList();
        }


    }
}
