﻿using Residency.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Residency.Services
{
    public class Common
    {
        static void Main(string[] args)
        {
        }

        public enum UserType
        {
            SuperAdmin = 1,
            Admin = 2,
            User = 3
        }
      

        /// <summary>
        /// Random String
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="mailTo"></param>
        /// <param name="Subject"></param>
        /// <param name="strBody"></param>
        /// <param name="strBcc"></param>
        /// <param name="StrCC"></param>
        /// <returns></returns>
        public static bool sendEmail(string mailTo, string Subject, string strBody, string strBcc, string StrCC)
        {
            bool result = false;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["CanEmailSend"]) == true)
            {
                var mailFrom = new MailAddress("eglesoftsolutions@gmail.com", "egle soft");

                var toAddress = new MailAddress(mailTo, Convert.ToString(mailTo.Split('@')[0]));
                string fromPassword = "Raman@5299";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(mailFrom.Address, fromPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(mailFrom, toAddress)
                {
                    Subject = Subject,
                    Body = strBody,
                    IsBodyHtml = true

                })
                {
                    if (!string.IsNullOrEmpty(StrCC))
                    {
                        message.CC.Add(StrCC);

                    }
                    if (!string.IsNullOrEmpty(strBcc))
                    {
                        message.Bcc.Add(strBcc);
                    }
                    try
                    {
                        smtp.Send(message);
                    }
                    catch (Exception)
                    {
                    }

                }


            }
            return result;

        }

        /// <summary>
        /// get Resource Value
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        public static string getResourceValue(string resourceName)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(resourceName))
            {
                ResidencyAgreementEntities db = new ResidencyAgreementEntities();
                var getDB = (from p in db.ResourceStriings where p.resourceName == resourceName && p.isDelete == false select p).ToList();
                if (getDB.Count > 0)
                {
                    result = Convert.ToString(getDB[0].resourceValue);
                }
            }
            return result;
        }

        /// <summary>
        /// Enter Error Log
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <param name="UserID"></param>
        public static void EnterLog(string errorMsg,int? UserID)
        {
            ResidencyAgreementEntities db = new ResidencyAgreementEntities();
            Log obj = new Log();
            obj.ErrorLogMsg = errorMsg;
            obj.UserId = UserID;
            db.Logs.Add(obj);
            db.SaveChanges();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string getDateFormat()
        {
          return "dd-mm-yyyy";
        }

    }

}
