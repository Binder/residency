﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Residency.Services.Model
{
   public  class facilityMenu
    {
        public int userID { get; set; }
        public int userRole { get; set; }
        public string facilityName { get; set; }
        public int facilityID { get; set; }
    }
}
