﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Residency.Services.Model
{
    public class GetFacilityDetailByUserID
    {

        /// <summary>
        /// Owner Account Setting
        /// </summary>
        public string ownerAccountSett_Name { get; set; }
        public string ownerAccountSett_Address { get; set; }
        public string ownerAccountSett_Suburb { get; set; }
        public string ownerAccountSett_State { get; set; }
        public string ownerAccountSett_PostalCode { get; set; }

        /// <summary>
        /// Account Setting
        /// </summary>
        public string AccountSettingSett_Name { get; set; }
        public string AccountSetting_Address { get; set; }
        public string AccountSetting_Suburb { get; set; }
        public string AccountSetting_State { get; set; }
        public string AccountSetting_PostalCode { get; set; }
        public string AccountSetting_ABN { get; set; }
        public string AccountSetting_ImageURL { get; set; }


    }
}
