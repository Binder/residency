﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Residency.Services.Model
{
    public class SavedDocument
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Progress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FacilityName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DocumentID { get; set; }

    }
}
