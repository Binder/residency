﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Residency.Services.FacilitiesForms
{
   public class FacilitiesOwnerDetail
    {
        public int FacilitiesID { get; set; }
        public string userName { get; set; }
        public string strPassowrd { get; set; }
        public string accountManager { get; set; }
        public string strPhoneContact { get; set; }
        public string strEmailContact { get; set; }
        public DateTime membershipExpiry { get; set; }
        public string strAddress { get; set; }
        public string strPostalCode { get; set; }
        public int StateID { get; set; }
        public string strCountry { get; set; }
        public string ProviderName { get; set; }
        public string suburb { get; set; }
    }
}
