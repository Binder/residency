﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Residency.Data;
using Residency.Services.Model;

namespace Residency.Services
{
    public class LoginService : ILoginService
    {
        #region Feilds
        ResidencyAgreementEntities db = new ResidencyAgreementEntities();
        #endregion

        #region Methods

        /// <summary>
        /// Is Valid User
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User getUserIDByUserName(string userName, string password)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                var q = from p in db.Users where p.userName == userName && p.password == password && p.isAcive == true && p.isDelete == false select p;
                if (q.Count() > 0)
                {
                    return q.SingleOrDefault();
                }
            }
            return null;
        }

        /// <summary>
        /// Is User Exist
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool IsUserExist(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var q = from p in db.Users where p.userName == userName select p;
                if (q.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            return true;
        }

        /// <summary>
        /// save Users
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool saveUsers(User obj)
        {
            try
            {
                db.Users.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// save Facility
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool saveFacility(Facility obj)
        {
            try
            {
                db.Facilities.Add(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Update Facility
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateFacility(Facility obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool deleteFacilityByID(int ID)
        {
            try
            {
                db.Facilities.Remove(getFacilityByID(ID));
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// get Facility By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Facility getFacilityByID(int ID)
        {
            var q = from p in db.Facilities where p.id == ID select p;
            if (q.Count() > 0)
            {
                return q.FirstOrDefault();
            }
            return null;
        }
        /// <summary>
        /// Update Users
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateUser(User obj)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// get All Users
        /// </summary>
        /// <returns></returns>
        public IList<User> getAllUsers(int? userTypeID)
        {
            if (userTypeID == null)
            {
                return (from p in db.Users where p.isDelete == false select p).ToList();
            }
            else
            {
                return (from p in db.Users where p.isDelete == false && p.userTypeID == userTypeID select p).ToList();
            }
        }

        /// <summary>
        /// get Account User By ID
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User getAccountUserByID(int ID)
        {
            if (ID > 0)
            {
                var q = from p in db.Users where p.ID == ID select p;
                if (q.Count() > 0)
                {
                    return q.SingleOrDefault();
                }
            }
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public IList<facilityMenu> getFacilityByUserID(int ID)
        {
            if (ID > 0)
            {
                return (from p in db.Users
                        join f in db.Facilities on p.ID equals f.userID into fa
                        from fac in fa.DefaultIfEmpty()
                        where p.ID == ID
                        select new facilityMenu
                        {
                            facilityName = fac.facilityName,
                            userID = p.ID,
                            userRole = (int)p.userTypeID,
                            facilityID = (fac == null ? 0 : fac.id)
                        }).ToList();

            }
            return null;
        }





        /// <summary>
        /// get Super Admin Email Id
        /// </summary>
        /// <returns></returns>
        public string getSuperAdminEmailId()
        {
            string emailId = string.Empty;
            var getUserId = (from p in db.Users where p.isDelete == false && p.userTypeID == (int)Common.UserType.SuperAdmin select p).ToList();
            if (getUserId.Count > 0)
            {
                emailId = getUserId[0].emailAddress;
            }
            return emailId;

        }

        /// <summary>
        /// save Picture
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool savePicture(Picture obj)
        {
            try
            {
                db.Pictures.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }



        #endregion

    }
}
