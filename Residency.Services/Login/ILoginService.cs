﻿using Residency.Data;
using Residency.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Residency.Services
{
    public interface ILoginService
    {
        /// <summary>
        /// Is Valid
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        User getUserIDByUserName(string userName,string password);

        /// <summary>
        /// save Users
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool saveUsers(User obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        bool IsUserExist(string userName);

        /// <summary>
        /// get All Users
        /// </summary>
        /// <returns></returns>
        IList<User> getAllUsers(int? userTypeID);

        /// <summary>
        /// get Account User By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        User getAccountUserByID(int ID);

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdateUser(User obj);

        /// <summary>
        /// get Super Admin Email Id
        /// </summary>
        /// <returns></returns>
        string getSuperAdminEmailId();

        /// <summary>
        /// save Facility
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool saveFacility(Facility obj);

        /// <summary>
        /// Update Facility
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool UpdateFacility(Facility obj);

        /// <summary>
        /// get Facility By User ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        IList<facilityMenu> getFacilityByUserID(int ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Facility getFacilityByID(int ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool deleteFacilityByID(int ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool savePicture(Picture obj);
    }
}
